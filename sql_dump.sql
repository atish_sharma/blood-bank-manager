-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 19, 2015 at 09:06 PM
-- Server version: 5.7.9
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `susanoo_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `bankowner`
--

CREATE TABLE `bankowner` (
  `bank_id` bigint(20) NOT NULL DEFAULT '0',
  `owner` varchar(300) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bankowner`
--

INSERT INTO `bankowner` (`bank_id`, `owner`) VALUES
(1, 'IBM Medical Solutions'),
(1, 'KMC'),
(2, 'IBM Medical Solutions'),
(2, 'Maharashtra Government'),
(2, 'Trinity');

-- --------------------------------------------------------

--
-- Table structure for table `bankvault`
--

CREATE TABLE `bankvault` (
  `bank_id` bigint(20) NOT NULL DEFAULT '0',
  `vault_name` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bankvault`
--

INSERT INTO `bankvault` (`bank_id`, `vault_name`) VALUES
(1, 'Manipal Blood Vault'),
(2, 'Trinity Mumbai Blood Vault');

-- --------------------------------------------------------

--
-- Table structure for table `bloodbank`
--

CREATE TABLE `bloodbank` (
  `bank_id` bigint(20) NOT NULL,
  `street` varchar(300) DEFAULT NULL,
  `pincode` varchar(300) DEFAULT NULL,
  `city` varchar(300) DEFAULT NULL,
  `state` varchar(300) DEFAULT NULL,
  `district` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bloodbank`
--

INSERT INTO `bloodbank` (`bank_id`, `street`, `pincode`, `city`, `state`, `district`) VALUES
(1, 'neon street', '100100', 'udupi', 'karnataka', 'manipal'),
(2, 'noble street', '300111', 'mumbai', 'maharashtra', 'thane');

-- --------------------------------------------------------

--
-- Table structure for table `bloodvault`
--

CREATE TABLE `bloodvault` (
  `bank_id` bigint(20) NOT NULL DEFAULT '0',
  `blood_type` varchar(300) NOT NULL DEFAULT '',
  `blood_volume` decimal(12,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bloodvault`
--

INSERT INTO `bloodvault` (`bank_id`, `blood_type`, `blood_volume`) VALUES
(1, 'A+', 120.00),
(1, 'A-', 150.00),
(1, 'AB+', 210.00),
(1, 'AB-', 190.00),
(1, 'B+', 200.00),
(1, 'B-', 200.00),
(1, 'O+', 200.00),
(1, 'O-', 200.00),
(2, 'A+', 200.00),
(2, 'A-', 200.00),
(2, 'AB+', 200.00),
(2, 'AB-', 300.00),
(2, 'B+', 120.00),
(2, 'B-', 200.00),
(2, 'O+', 200.00),
(2, 'O-', 150.00);

-- --------------------------------------------------------

--
-- Table structure for table `donor`
--

CREATE TABLE `donor` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `volume_donated` decimal(12,2) DEFAULT NULL,
  `timestamp_donated` bigint(20) NOT NULL DEFAULT '0',
  `bank_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `owneraddress`
--

CREATE TABLE `owneraddress` (
  `owner` varchar(300) NOT NULL DEFAULT '',
  `state` varchar(300) DEFAULT NULL,
  `city` varchar(300) DEFAULT NULL,
  `district` varchar(300) DEFAULT NULL,
  `pincode` varchar(300) DEFAULT NULL,
  `street` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `owneraddress`
--

INSERT INTO `owneraddress` (`owner`, `state`, `city`, `district`, `pincode`, `street`) VALUES
('IBM Medical Solutions', 'delhi', 'delhi', 'north delhi', '100101', 'west side street'),
('Maharashtra Government', 'maharashtra', 'mumbai', 'thane', '111300', 'noble street'),
('Trinity', 'karnataka', 'mumbai', 'thane', '111300', 'noble street');

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE `person` (
  `id` bigint(20) NOT NULL,
  `firstname` varchar(300) DEFAULT NULL,
  `lastname` varchar(300) DEFAULT NULL,
  `dob` varchar(300) DEFAULT NULL,
  `blood_type` varchar(300) DEFAULT NULL,
  `street` varchar(300) DEFAULT NULL,
  `pincode` varchar(300) DEFAULT NULL,
  `city` varchar(300) DEFAULT NULL,
  `district` varchar(300) DEFAULT NULL,
  `state` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`id`, `firstname`, `lastname`, `dob`, `blood_type`, `street`, `pincode`, `city`, `district`, `state`) VALUES
(1, 'ash', 'yo', '1987-11-11', 'AB+', 'street', 'pincode', 'random city', 'hmmm...', 'god knows'),
(2, 'kay', 'pal', '1991-01-01', 'A-', 'no one knows', '----', '-_-', ':/', '+='),
(3, 'ash', 'n', 'Thursday, November 19, 2015', 'O+', '123', '1', '34', '1', '1'),
(4, 'Atish', 'Sharma', 'Thursday, November 19, 2015', 'O+', '1', '1', '1', '1', '1'),
(5, 'Atish Again', ';?', 'Thursday, November 19, 2015', 'O+', '1', '3', '2', '4', '5'),
(6, 'ash', 'yo', 'thursday, november 19, 2015', 'o+', '1', '1', '1', '1', '1'),
(7, 'atish', 'sharma', 'Tuesday, September 12, 1995', 'O+', 'watson street', '169001', 'wulburry', 'west', 'lancshire'),
(8, 'naruto', 'uzumaki', 'Thursday, November 19, 2015', 'O+', 'ab', 'v', 'b', 'd', 'd'),
(9, 'naruto', 'uzumaki', 'Thursday, November 19, 2015', 'O+', 'a', 'v', 'b', 'd', 's'),
(10, 'ash', 'l', 'Thursday, November 19, 2015', 'O+', '1', '3', '2', '4', '5');

-- --------------------------------------------------------

--
-- Table structure for table `personemail`
--

CREATE TABLE `personemail` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `email` varchar(300) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `personemail`
--

INSERT INTO `personemail` (`id`, `email`) VALUES
(9, 'a@k'),
(9, 'f@4'),
(9, 'w@f'),
(10, 'a'),
(10, 'b'),
(10, 'c'),
(10, 'd'),
(10, 'f');

-- --------------------------------------------------------

--
-- Table structure for table `personphone`
--

CREATE TABLE `personphone` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `phone` varchar(300) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `personphone`
--

INSERT INTO `personphone` (`id`, `phone`) VALUES
(9, '123'),
(9, '45'),
(9, '78'),
(9, '99'),
(10, '1'),
(10, '2'),
(10, '3'),
(10, '4');

-- --------------------------------------------------------

--
-- Table structure for table `recipient`
--

CREATE TABLE `recipient` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `volume_received` decimal(12,2) DEFAULT NULL,
  `timestamp_received` bigint(20) NOT NULL DEFAULT '0',
  `bank_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bankowner`
--
ALTER TABLE `bankowner`
  ADD PRIMARY KEY (`bank_id`,`owner`);

--
-- Indexes for table `bankvault`
--
ALTER TABLE `bankvault`
  ADD PRIMARY KEY (`bank_id`);

--
-- Indexes for table `bloodbank`
--
ALTER TABLE `bloodbank`
  ADD PRIMARY KEY (`bank_id`);

--
-- Indexes for table `bloodvault`
--
ALTER TABLE `bloodvault`
  ADD PRIMARY KEY (`bank_id`,`blood_type`);

--
-- Indexes for table `donor`
--
ALTER TABLE `donor`
  ADD PRIMARY KEY (`id`,`timestamp_donated`);

--
-- Indexes for table `owneraddress`
--
ALTER TABLE `owneraddress`
  ADD PRIMARY KEY (`owner`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personemail`
--
ALTER TABLE `personemail`
  ADD PRIMARY KEY (`id`,`email`);

--
-- Indexes for table `personphone`
--
ALTER TABLE `personphone`
  ADD PRIMARY KEY (`id`,`phone`);

--
-- Indexes for table `recipient`
--
ALTER TABLE `recipient`
  ADD PRIMARY KEY (`id`,`timestamp_received`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bloodbank`
--
ALTER TABLE `bloodbank`
  MODIFY `bank_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `person`
--
ALTER TABLE `person`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;