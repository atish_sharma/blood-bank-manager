-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 16, 2015 at 07:29 AM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `blood_bank`
--

-- --------------------------------------------------------

--
-- Table structure for table `bankowner`
--

CREATE TABLE IF NOT EXISTS `bankowner` (
  `bank_id` bigint(20) NOT NULL DEFAULT '0',
  `owner` varchar(300) NOT NULL DEFAULT '',
  PRIMARY KEY (`bank_id`,`owner`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bankvault`
--

CREATE TABLE IF NOT EXISTS `bankvault` (
  `bank_id` bigint(20) NOT NULL DEFAULT '0',
  `vault_name` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`bank_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bloodbank`
--

CREATE TABLE IF NOT EXISTS `bloodbank` (
  `bank_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `street` varchar(300) DEFAULT NULL,
  `pincode` varchar(300) DEFAULT NULL,
  `city` varchar(300) DEFAULT NULL,
  `state` varchar(300) DEFAULT NULL,
  `district` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`bank_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `bloodvault`
--

CREATE TABLE IF NOT EXISTS `bloodvault` (
  `bank_id` bigint(20) NOT NULL DEFAULT '0',
  `blood_type` varchar(300) NOT NULL DEFAULT '',
  `blood_volume` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`bank_id`,`blood_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `donor`
--

CREATE TABLE IF NOT EXISTS `donor` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `volume_donated` decimal(12,2) DEFAULT NULL,
  `timestamp_donated` bigint(20) NOT NULL DEFAULT '0',
  `bank_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`,`timestamp_donated`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `owneraddress`
--

CREATE TABLE IF NOT EXISTS `owneraddress` (
  `owner` varchar(300) NOT NULL DEFAULT '',
  `state` varchar(300) DEFAULT NULL,
  `city` varchar(300) DEFAULT NULL,
  `district` varchar(300) DEFAULT NULL,
  `pincode` varchar(300) DEFAULT NULL,
  `street` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`owner`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE IF NOT EXISTS `person` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(300) DEFAULT NULL,
  `lastname` varchar(300) DEFAULT NULL,
  `dob` varchar(300) DEFAULT NULL,
  `blood_type` varchar(300) DEFAULT NULL,
  `street` varchar(300) DEFAULT NULL,
  `pincode` varchar(300) DEFAULT NULL,
  `city` varchar(300) DEFAULT NULL,
  `district` varchar(300) DEFAULT NULL,
  `state` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `personemail`
--

CREATE TABLE IF NOT EXISTS `personemail` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `email` varchar(300) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `personphone`
--

CREATE TABLE IF NOT EXISTS `personphone` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `phone` varchar(300) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `recipient`
--

CREATE TABLE IF NOT EXISTS `recipient` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `volume_received` decimal(12,2) DEFAULT NULL,
  `timestamp_received` bigint(20) NOT NULL DEFAULT '0',
  `bank_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`,`timestamp_received`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
