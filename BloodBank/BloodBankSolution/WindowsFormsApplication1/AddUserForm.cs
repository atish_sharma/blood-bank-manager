﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Animations;
using MaterialSkin.Controls;
using MySql.Data.MySqlClient;

namespace BloodBank
{
    public partial class AddUserForm : MaterialForm
    {

        private AsyncResultManager mAsyncResultManager;
        private DatabaseManager mDatabaseManager;
        private string firstName, lastName, street, pinCode, district, state, bloodType, dob, city, email, phone;
        private Boolean success;
        private string lastInsertedId;

        public AddUserForm()
        {
            InitializeComponent();
            DesignManager.setDesign(this);
            mDatabaseManager = DatabaseManager.getInstance();
            mAsyncResultManager = AsyncResultManager.getInstance();
            mAddUserBloodType.Items.AddRange(MainForm.mBloodType);
            mAddUserBloodType.SelectedIndex = 0;
        }

        private async void mCreateNewUser_Click(object sender, EventArgs e)
        {
            firstName = mNewUserFirstName.Text.Trim().ToLower();
            lastName = mNewUserLastName.Text.Trim().ToLower();
            dob = mAddUserDobPicker.Text.Trim();
            bloodType = mAddUserBloodType.SelectedItem.ToString();
            street = mNewUserStreet.Text.Trim().ToLower();
            pinCode = mNewUserPinCode.Text.Trim().ToLower();
            district = mNewUserDistrict.Text.Trim().ToLower();
            state = mNewUserState.Text.Trim().ToLower();
            city = mNewUserCity.Text.Trim().ToLower();
            email = mUserEmailId.Text.Trim().ToLower();
            phone = mUserPhoneNumber.Text.Trim().ToLower();
            Boolean error = false;
            string err = "";
            if (firstName.Equals(""))
            {
                error = true;
                err += "Please enter your first name.\n";
            }
            if(lastName.Equals(""))
            {
                error = true;
                err += "Please enter your last name.\n";
            }
            if (dob.Equals(""))
            {
                error = true;
                err += "Please enter your date of birth.\n";
            }
            if (bloodType.Equals(""))
            {
                error = true;
                err += "Please enter your blood type.\n";
            }
            if (street.Equals(""))
            {
                error = true;
                err += "Please enter your street.\n";
            }
            if (pinCode.Equals(""))
            {
                error = true;
                err += "Please enter your pin code.\n";
            }
            if (district.Equals(""))
            {
                error = true;
                err += "Please enter your district.\n";
            }
            if (city.Equals(""))
            {
                error = true;
                err += "Please enter your city.\n";
            }
            if (state.Equals(""))
            {
                error = true;
                err += "Please enter your state.\n";
            }
            if (email.Equals(""))
            {
                error = true;
                err += "Please enter your email address.\n";
            }
            if (phone.Equals(""))
            {
                error = true;
                err += "Please enter your phone number.\n";
            }
            if(error)
            {
                DesignManager.message("Error", err, MessageBoxIcon.Error);
                return;
            }
            success = false;
            mNewUserProgress.Visible = true;
            await Task.Factory.StartNew(() => addUser(), TaskCreationOptions.LongRunning);
            mNewUserProgress.Visible = false;
            if(success)
            {
                DesignManager.message("New User Created", String.Format("User Id : {0}", lastInsertedId),MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                DesignManager.message("Error", "Unable to query database! Please check your internet connection.", MessageBoxIcon.Error);
            }
        }

        private void addUser()
        {
            MySqlConnection conn = mDatabaseManager.getConnection();
            if (conn == null)
            {
                return;
            }
            try
            {
                string stm = String.Format("insert into person (firstname,lastname,dob,blood_type,street,pincode,city,district,state) values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')",firstName,lastName,dob,bloodType,street,pinCode,city,district,state);
                MySqlCommand cmd = new MySqlCommand(stm, conn);
                cmd.ExecuteNonQuery();
                //stm = "SELECT LAST_INSERT_ID()";
                stm = "CALL LII()";
                cmd = new MySqlCommand(stm, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                while(rdr.Read())
                {
                    lastInsertedId = rdr.GetString(0);
                }
                rdr.Close();
                long id = Int64.Parse(lastInsertedId);
                string[] phoneNums = phone.Split(',');
                string[] emailIds = email.Split(',');
                foreach(string s in phoneNums)
                {
                    stm = String.Format("insert into personphone (id,phone) values ({0},'{1}')",id,s.Trim().ToLower());
                    cmd = new MySqlCommand(stm, conn);
                    cmd.ExecuteNonQuery();
                }
                foreach (string s in emailIds)
                {
                    stm = String.Format("insert into personemail (id,email) values ({0},'{1}')", id, s.Trim().ToLower());
                    cmd = new MySqlCommand(stm, conn);
                    cmd.ExecuteNonQuery();
                }
                success = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                success = false;
            }
            mDatabaseManager.closeConnection();
        }


    }
}
