﻿namespace BloodBank
{
    partial class AddUserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddUserForm));
            this.mNewUserFirstName = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.mNewUserCity = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.mNewUserPinCode = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.mNewUserDistrict = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.mNewUserState = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.mNewUserStreet = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.mNewUserLastName = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.mCreateNewUser = new MaterialSkin.Controls.MaterialRaisedButton();
            this.mAddUserDobPicker = new System.Windows.Forms.DateTimePicker();
            this.mAddUserDobLabel = new MaterialSkin.Controls.MaterialLabel();
            this.mAddUserBloodType = new System.Windows.Forms.ComboBox();
            this.mAddUserBloodTypeLabel = new MaterialSkin.Controls.MaterialLabel();
            this.mNewUserProgress = new System.Windows.Forms.ProgressBar();
            this.mUserPhoneNumber = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.mUserEmailId = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.SuspendLayout();
            // 
            // mNewUserFirstName
            // 
            this.mNewUserFirstName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mNewUserFirstName.Depth = 0;
            this.mNewUserFirstName.Hint = "First Name";
            this.mNewUserFirstName.Location = new System.Drawing.Point(12, 84);
            this.mNewUserFirstName.MouseState = MaterialSkin.MouseState.HOVER;
            this.mNewUserFirstName.Name = "mNewUserFirstName";
            this.mNewUserFirstName.PasswordChar = '\0';
            this.mNewUserFirstName.SelectedText = "";
            this.mNewUserFirstName.SelectionLength = 0;
            this.mNewUserFirstName.SelectionStart = 0;
            this.mNewUserFirstName.Size = new System.Drawing.Size(292, 23);
            this.mNewUserFirstName.TabIndex = 0;
            this.mNewUserFirstName.UseSystemPasswordChar = false;
            // 
            // mNewUserCity
            // 
            this.mNewUserCity.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mNewUserCity.Depth = 0;
            this.mNewUserCity.Hint = "City";
            this.mNewUserCity.Location = new System.Drawing.Point(12, 245);
            this.mNewUserCity.MouseState = MaterialSkin.MouseState.HOVER;
            this.mNewUserCity.Name = "mNewUserCity";
            this.mNewUserCity.PasswordChar = '\0';
            this.mNewUserCity.SelectedText = "";
            this.mNewUserCity.SelectionLength = 0;
            this.mNewUserCity.SelectionStart = 0;
            this.mNewUserCity.Size = new System.Drawing.Size(292, 23);
            this.mNewUserCity.TabIndex = 1;
            this.mNewUserCity.UseSystemPasswordChar = false;
            // 
            // mNewUserPinCode
            // 
            this.mNewUserPinCode.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mNewUserPinCode.Depth = 0;
            this.mNewUserPinCode.Hint = "Pin Code";
            this.mNewUserPinCode.Location = new System.Drawing.Point(12, 289);
            this.mNewUserPinCode.MouseState = MaterialSkin.MouseState.HOVER;
            this.mNewUserPinCode.Name = "mNewUserPinCode";
            this.mNewUserPinCode.PasswordChar = '\0';
            this.mNewUserPinCode.SelectedText = "";
            this.mNewUserPinCode.SelectionLength = 0;
            this.mNewUserPinCode.SelectionStart = 0;
            this.mNewUserPinCode.Size = new System.Drawing.Size(292, 23);
            this.mNewUserPinCode.TabIndex = 2;
            this.mNewUserPinCode.UseSystemPasswordChar = false;
            // 
            // mNewUserDistrict
            // 
            this.mNewUserDistrict.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mNewUserDistrict.Depth = 0;
            this.mNewUserDistrict.Hint = "District";
            this.mNewUserDistrict.Location = new System.Drawing.Point(324, 245);
            this.mNewUserDistrict.MouseState = MaterialSkin.MouseState.HOVER;
            this.mNewUserDistrict.Name = "mNewUserDistrict";
            this.mNewUserDistrict.PasswordChar = '\0';
            this.mNewUserDistrict.SelectedText = "";
            this.mNewUserDistrict.SelectionLength = 0;
            this.mNewUserDistrict.SelectionStart = 0;
            this.mNewUserDistrict.Size = new System.Drawing.Size(292, 23);
            this.mNewUserDistrict.TabIndex = 3;
            this.mNewUserDistrict.UseSystemPasswordChar = false;
            // 
            // mNewUserState
            // 
            this.mNewUserState.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mNewUserState.Depth = 0;
            this.mNewUserState.Hint = "State";
            this.mNewUserState.Location = new System.Drawing.Point(324, 289);
            this.mNewUserState.MouseState = MaterialSkin.MouseState.HOVER;
            this.mNewUserState.Name = "mNewUserState";
            this.mNewUserState.PasswordChar = '\0';
            this.mNewUserState.SelectedText = "";
            this.mNewUserState.SelectionLength = 0;
            this.mNewUserState.SelectionStart = 0;
            this.mNewUserState.Size = new System.Drawing.Size(292, 23);
            this.mNewUserState.TabIndex = 4;
            this.mNewUserState.UseSystemPasswordChar = false;
            // 
            // mNewUserStreet
            // 
            this.mNewUserStreet.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mNewUserStreet.Depth = 0;
            this.mNewUserStreet.Hint = "Street";
            this.mNewUserStreet.Location = new System.Drawing.Point(12, 199);
            this.mNewUserStreet.MouseState = MaterialSkin.MouseState.HOVER;
            this.mNewUserStreet.Name = "mNewUserStreet";
            this.mNewUserStreet.PasswordChar = '\0';
            this.mNewUserStreet.SelectedText = "";
            this.mNewUserStreet.SelectionLength = 0;
            this.mNewUserStreet.SelectionStart = 0;
            this.mNewUserStreet.Size = new System.Drawing.Size(604, 23);
            this.mNewUserStreet.TabIndex = 5;
            this.mNewUserStreet.UseSystemPasswordChar = false;
            // 
            // mNewUserLastName
            // 
            this.mNewUserLastName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mNewUserLastName.Depth = 0;
            this.mNewUserLastName.Hint = "Last Name";
            this.mNewUserLastName.Location = new System.Drawing.Point(324, 84);
            this.mNewUserLastName.MouseState = MaterialSkin.MouseState.HOVER;
            this.mNewUserLastName.Name = "mNewUserLastName";
            this.mNewUserLastName.PasswordChar = '\0';
            this.mNewUserLastName.SelectedText = "";
            this.mNewUserLastName.SelectionLength = 0;
            this.mNewUserLastName.SelectionStart = 0;
            this.mNewUserLastName.Size = new System.Drawing.Size(292, 23);
            this.mNewUserLastName.TabIndex = 6;
            this.mNewUserLastName.UseSystemPasswordChar = false;
            // 
            // mCreateNewUser
            // 
            this.mCreateNewUser.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mCreateNewUser.Depth = 0;
            this.mCreateNewUser.Location = new System.Drawing.Point(12, 449);
            this.mCreateNewUser.MouseState = MaterialSkin.MouseState.HOVER;
            this.mCreateNewUser.Name = "mCreateNewUser";
            this.mCreateNewUser.Primary = true;
            this.mCreateNewUser.Size = new System.Drawing.Size(75, 23);
            this.mCreateNewUser.TabIndex = 7;
            this.mCreateNewUser.Text = "Submit";
            this.mCreateNewUser.UseVisualStyleBackColor = true;
            this.mCreateNewUser.Click += new System.EventHandler(this.mCreateNewUser_Click);
            // 
            // mAddUserDobPicker
            // 
            this.mAddUserDobPicker.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mAddUserDobPicker.Location = new System.Drawing.Point(124, 128);
            this.mAddUserDobPicker.Name = "mAddUserDobPicker";
            this.mAddUserDobPicker.Size = new System.Drawing.Size(492, 20);
            this.mAddUserDobPicker.TabIndex = 8;
            // 
            // mAddUserDobLabel
            // 
            this.mAddUserDobLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mAddUserDobLabel.AutoSize = true;
            this.mAddUserDobLabel.Depth = 0;
            this.mAddUserDobLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.mAddUserDobLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mAddUserDobLabel.Location = new System.Drawing.Point(12, 130);
            this.mAddUserDobLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.mAddUserDobLabel.Name = "mAddUserDobLabel";
            this.mAddUserDobLabel.Size = new System.Drawing.Size(93, 19);
            this.mAddUserDobLabel.TabIndex = 9;
            this.mAddUserDobLabel.Text = "Date of Birth";
            // 
            // mAddUserBloodType
            // 
            this.mAddUserBloodType.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mAddUserBloodType.FormattingEnabled = true;
            this.mAddUserBloodType.Location = new System.Drawing.Point(124, 163);
            this.mAddUserBloodType.Name = "mAddUserBloodType";
            this.mAddUserBloodType.Size = new System.Drawing.Size(492, 21);
            this.mAddUserBloodType.TabIndex = 10;
            // 
            // mAddUserBloodTypeLabel
            // 
            this.mAddUserBloodTypeLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mAddUserBloodTypeLabel.AutoSize = true;
            this.mAddUserBloodTypeLabel.Depth = 0;
            this.mAddUserBloodTypeLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.mAddUserBloodTypeLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mAddUserBloodTypeLabel.Location = new System.Drawing.Point(12, 162);
            this.mAddUserBloodTypeLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.mAddUserBloodTypeLabel.Name = "mAddUserBloodTypeLabel";
            this.mAddUserBloodTypeLabel.Size = new System.Drawing.Size(84, 19);
            this.mAddUserBloodTypeLabel.TabIndex = 11;
            this.mAddUserBloodTypeLabel.Text = "Blood Type";
            // 
            // mNewUserProgress
            // 
            this.mNewUserProgress.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mNewUserProgress.Location = new System.Drawing.Point(12, 411);
            this.mNewUserProgress.MarqueeAnimationSpeed = 10;
            this.mNewUserProgress.Name = "mNewUserProgress";
            this.mNewUserProgress.Size = new System.Drawing.Size(604, 23);
            this.mNewUserProgress.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.mNewUserProgress.TabIndex = 12;
            this.mNewUserProgress.Visible = false;
            // 
            // mUserPhoneNumber
            // 
            this.mUserPhoneNumber.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mUserPhoneNumber.Depth = 0;
            this.mUserPhoneNumber.Hint = "Phone Number (separate by , if more than one)";
            this.mUserPhoneNumber.Location = new System.Drawing.Point(12, 329);
            this.mUserPhoneNumber.MouseState = MaterialSkin.MouseState.HOVER;
            this.mUserPhoneNumber.Name = "mUserPhoneNumber";
            this.mUserPhoneNumber.PasswordChar = '\0';
            this.mUserPhoneNumber.SelectedText = "";
            this.mUserPhoneNumber.SelectionLength = 0;
            this.mUserPhoneNumber.SelectionStart = 0;
            this.mUserPhoneNumber.Size = new System.Drawing.Size(604, 23);
            this.mUserPhoneNumber.TabIndex = 13;
            this.mUserPhoneNumber.UseSystemPasswordChar = false;
            // 
            // mUserEmailId
            // 
            this.mUserEmailId.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mUserEmailId.Depth = 0;
            this.mUserEmailId.Hint = "Email Id (separate by , if more than one)";
            this.mUserEmailId.Location = new System.Drawing.Point(12, 373);
            this.mUserEmailId.MouseState = MaterialSkin.MouseState.HOVER;
            this.mUserEmailId.Name = "mUserEmailId";
            this.mUserEmailId.PasswordChar = '\0';
            this.mUserEmailId.SelectedText = "";
            this.mUserEmailId.SelectionLength = 0;
            this.mUserEmailId.SelectionStart = 0;
            this.mUserEmailId.Size = new System.Drawing.Size(604, 23);
            this.mUserEmailId.TabIndex = 14;
            this.mUserEmailId.UseSystemPasswordChar = false;
            // 
            // AddUserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(628, 509);
            this.Controls.Add(this.mUserEmailId);
            this.Controls.Add(this.mUserPhoneNumber);
            this.Controls.Add(this.mNewUserProgress);
            this.Controls.Add(this.mAddUserBloodTypeLabel);
            this.Controls.Add(this.mAddUserBloodType);
            this.Controls.Add(this.mAddUserDobLabel);
            this.Controls.Add(this.mAddUserDobPicker);
            this.Controls.Add(this.mCreateNewUser);
            this.Controls.Add(this.mNewUserLastName);
            this.Controls.Add(this.mNewUserStreet);
            this.Controls.Add(this.mNewUserState);
            this.Controls.Add(this.mNewUserDistrict);
            this.Controls.Add(this.mNewUserPinCode);
            this.Controls.Add(this.mNewUserCity);
            this.Controls.Add(this.mNewUserFirstName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddUserForm";
            this.Text = "New User";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialSingleLineTextField mNewUserFirstName;
        private MaterialSkin.Controls.MaterialSingleLineTextField mNewUserCity;
        private MaterialSkin.Controls.MaterialSingleLineTextField mNewUserPinCode;
        private MaterialSkin.Controls.MaterialSingleLineTextField mNewUserDistrict;
        private MaterialSkin.Controls.MaterialSingleLineTextField mNewUserState;
        private MaterialSkin.Controls.MaterialSingleLineTextField mNewUserStreet;
        private MaterialSkin.Controls.MaterialSingleLineTextField mNewUserLastName;
        private MaterialSkin.Controls.MaterialRaisedButton mCreateNewUser;
        private System.Windows.Forms.DateTimePicker mAddUserDobPicker;
        private MaterialSkin.Controls.MaterialLabel mAddUserDobLabel;
        private System.Windows.Forms.ComboBox mAddUserBloodType;
        private MaterialSkin.Controls.MaterialLabel mAddUserBloodTypeLabel;
        private System.Windows.Forms.ProgressBar mNewUserProgress;
        private MaterialSkin.Controls.MaterialSingleLineTextField mUserPhoneNumber;
        private MaterialSkin.Controls.MaterialSingleLineTextField mUserEmailId;
    }
}