﻿namespace BloodBank
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.mLoginPanel = new System.Windows.Forms.Panel();
            this.mLoginLabel = new MaterialSkin.Controls.MaterialLabel();
            this.mInvalidPasswordLabel = new MaterialSkin.Controls.MaterialLabel();
            this.mLoginButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.mExitButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.mLoginPassword = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.mHomePanel = new System.Windows.Forms.Panel();
            this.mHomeTabContainer = new MaterialSkin.Controls.MaterialTabControl();
            this.mHomeTab = new System.Windows.Forms.TabPage();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.mUserTab = new System.Windows.Forms.TabPage();
            this.mUserSearchResultList = new System.Windows.Forms.ListView();
            this.id = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.firstName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lastName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dob = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bloodType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.street = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pinCode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.city = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.district = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.state = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mSearchUserLabel = new MaterialSkin.Controls.MaterialLabel();
            this.mAddUserButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.mUserSearchProgress = new System.Windows.Forms.ProgressBar();
            this.mClearUserSearch = new MaterialSkin.Controls.MaterialRaisedButton();
            this.mSearchUserByLabel = new MaterialSkin.Controls.MaterialLabel();
            this.mSearchUser = new MaterialSkin.Controls.MaterialRaisedButton();
            this.mUserSearchOptions = new System.Windows.Forms.ComboBox();
            this.mUserSearchField = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.mBloodBankTab = new System.Windows.Forms.TabPage();
            this.mBloodBankSearch = new MaterialSkin.Controls.MaterialRaisedButton();
            this.mBloodBankName = new MaterialSkin.Controls.MaterialLabel();
            this.mAcquireProgress = new System.Windows.Forms.ProgressBar();
            this.mSearchProgress = new System.Windows.Forms.ProgressBar();
            this.mUserId = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.mAcquire = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            this.mBloodBankCity = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.mBloodVolume = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.mBloodBankList = new System.Windows.Forms.ListView();
            this.bb_name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bb_type = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bb_vol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bb_street = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bb_district = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bb_pincode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bb_city = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bb_state = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mBloodBankState = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.mBloodBankBloodType = new System.Windows.Forms.ComboBox();
            this.mSearchByLocation = new MaterialSkin.Controls.MaterialCheckBox();
            this.mSettingsTab = new System.Windows.Forms.TabPage();
            this.mExitSettings = new MaterialSkin.Controls.MaterialRaisedButton();
            this.mChangePassword = new MaterialSkin.Controls.MaterialRaisedButton();
            this.mLogOut = new MaterialSkin.Controls.MaterialRaisedButton();
            this.mHomeTabSelector = new MaterialSkin.Controls.MaterialTabSelector();
            this.mLoginPanel.SuspendLayout();
            this.mHomePanel.SuspendLayout();
            this.mHomeTabContainer.SuspendLayout();
            this.mHomeTab.SuspendLayout();
            this.mUserTab.SuspendLayout();
            this.mBloodBankTab.SuspendLayout();
            this.mSettingsTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // mLoginPanel
            // 
            this.mLoginPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mLoginPanel.AutoScroll = true;
            this.mLoginPanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.mLoginPanel.Controls.Add(this.mLoginLabel);
            this.mLoginPanel.Controls.Add(this.mInvalidPasswordLabel);
            this.mLoginPanel.Controls.Add(this.mLoginButton);
            this.mLoginPanel.Controls.Add(this.mExitButton);
            this.mLoginPanel.Controls.Add(this.mLoginPassword);
            this.mLoginPanel.Location = new System.Drawing.Point(0, 64);
            this.mLoginPanel.Name = "mLoginPanel";
            this.mLoginPanel.Size = new System.Drawing.Size(628, 445);
            this.mLoginPanel.TabIndex = 0;
            this.mLoginPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.mLoginPanel_Paint);
            // 
            // mLoginLabel
            // 
            this.mLoginLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mLoginLabel.AutoSize = true;
            this.mLoginLabel.Depth = 0;
            this.mLoginLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.mLoginLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mLoginLabel.Location = new System.Drawing.Point(71, 98);
            this.mLoginLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.mLoginLabel.Name = "mLoginLabel";
            this.mLoginLabel.Size = new System.Drawing.Size(281, 19);
            this.mLoginLabel.TabIndex = 6;
            this.mLoginLabel.Text = "Enter password ( initial password : root )";
            // 
            // mInvalidPasswordLabel
            // 
            this.mInvalidPasswordLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mInvalidPasswordLabel.AutoSize = true;
            this.mInvalidPasswordLabel.Depth = 0;
            this.mInvalidPasswordLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.mInvalidPasswordLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mInvalidPasswordLabel.Location = new System.Drawing.Point(71, 224);
            this.mInvalidPasswordLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.mInvalidPasswordLabel.Name = "mInvalidPasswordLabel";
            this.mInvalidPasswordLabel.Size = new System.Drawing.Size(239, 19);
            this.mInvalidPasswordLabel.TabIndex = 5;
            this.mInvalidPasswordLabel.Text = "Invalid password! Please try again.";
            // 
            // mLoginButton
            // 
            this.mLoginButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mLoginButton.Depth = 0;
            this.mLoginButton.Location = new System.Drawing.Point(75, 184);
            this.mLoginButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.mLoginButton.Name = "mLoginButton";
            this.mLoginButton.Primary = true;
            this.mLoginButton.Size = new System.Drawing.Size(75, 23);
            this.mLoginButton.TabIndex = 3;
            this.mLoginButton.Text = "Log In";
            this.mLoginButton.UseVisualStyleBackColor = true;
            this.mLoginButton.Click += new System.EventHandler(this.mLoginButton_Click);
            // 
            // mExitButton
            // 
            this.mExitButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mExitButton.Depth = 0;
            this.mExitButton.Location = new System.Drawing.Point(169, 184);
            this.mExitButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.mExitButton.Name = "mExitButton";
            this.mExitButton.Primary = true;
            this.mExitButton.Size = new System.Drawing.Size(75, 23);
            this.mExitButton.TabIndex = 2;
            this.mExitButton.Text = "EXIT";
            this.mExitButton.UseVisualStyleBackColor = true;
            this.mExitButton.Click += new System.EventHandler(this.mExitButton_Click);
            // 
            // mLoginPassword
            // 
            this.mLoginPassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mLoginPassword.Depth = 0;
            this.mLoginPassword.Hint = "Password";
            this.mLoginPassword.Location = new System.Drawing.Point(75, 143);
            this.mLoginPassword.MouseState = MaterialSkin.MouseState.HOVER;
            this.mLoginPassword.Name = "mLoginPassword";
            this.mLoginPassword.PasswordChar = '\0';
            this.mLoginPassword.SelectedText = "";
            this.mLoginPassword.SelectionLength = 0;
            this.mLoginPassword.SelectionStart = 0;
            this.mLoginPassword.Size = new System.Drawing.Size(523, 23);
            this.mLoginPassword.TabIndex = 0;
            this.mLoginPassword.UseSystemPasswordChar = true;
            // 
            // mHomePanel
            // 
            this.mHomePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mHomePanel.BackColor = System.Drawing.SystemColors.Control;
            this.mHomePanel.Controls.Add(this.mHomeTabContainer);
            this.mHomePanel.Controls.Add(this.mHomeTabSelector);
            this.mHomePanel.Location = new System.Drawing.Point(0, 64);
            this.mHomePanel.Name = "mHomePanel";
            this.mHomePanel.Size = new System.Drawing.Size(628, 445);
            this.mHomePanel.TabIndex = 1;
            // 
            // mHomeTabContainer
            // 
            this.mHomeTabContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mHomeTabContainer.Controls.Add(this.mHomeTab);
            this.mHomeTabContainer.Controls.Add(this.mUserTab);
            this.mHomeTabContainer.Controls.Add(this.mBloodBankTab);
            this.mHomeTabContainer.Controls.Add(this.mSettingsTab);
            this.mHomeTabContainer.Depth = 0;
            this.mHomeTabContainer.Location = new System.Drawing.Point(0, 39);
            this.mHomeTabContainer.Margin = new System.Windows.Forms.Padding(0);
            this.mHomeTabContainer.MouseState = MaterialSkin.MouseState.HOVER;
            this.mHomeTabContainer.Name = "mHomeTabContainer";
            this.mHomeTabContainer.SelectedIndex = 0;
            this.mHomeTabContainer.Size = new System.Drawing.Size(628, 406);
            this.mHomeTabContainer.TabIndex = 1;
            // 
            // mHomeTab
            // 
            this.mHomeTab.BackColor = System.Drawing.SystemColors.Control;
            this.mHomeTab.Controls.Add(this.materialLabel4);
            this.mHomeTab.Controls.Add(this.materialLabel3);
            this.mHomeTab.Controls.Add(this.materialLabel2);
            this.mHomeTab.Controls.Add(this.materialLabel1);
            this.mHomeTab.Location = new System.Drawing.Point(4, 22);
            this.mHomeTab.Name = "mHomeTab";
            this.mHomeTab.Padding = new System.Windows.Forms.Padding(3);
            this.mHomeTab.Size = new System.Drawing.Size(620, 380);
            this.mHomeTab.TabIndex = 0;
            this.mHomeTab.Text = "Home";
            // 
            // materialLabel4
            // 
            this.materialLabel4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.materialLabel4.Depth = 0;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel4.Location = new System.Drawing.Point(8, 351);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(206, 24);
            this.materialLabel4.TabIndex = 3;
            this.materialLabel4.Text = "mehtajineshs@gmail.com";
            // 
            // materialLabel3
            // 
            this.materialLabel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(220, 351);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(206, 24);
            this.materialLabel3.TabIndex = 2;
            this.materialLabel3.Text = "atishsharma@live.in";
            // 
            // materialLabel2
            // 
            this.materialLabel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(8, 311);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(357, 28);
            this.materialLabel2.TabIndex = 1;
            this.materialLabel2.Text = "For further queries contact :";
            // 
            // materialLabel1
            // 
            this.materialLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(6, 14);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(604, 276);
            this.materialLabel1.TabIndex = 0;
            this.materialLabel1.Text = resources.GetString("materialLabel1.Text");
            // 
            // mUserTab
            // 
            this.mUserTab.AutoScroll = true;
            this.mUserTab.BackColor = System.Drawing.SystemColors.Control;
            this.mUserTab.Controls.Add(this.mUserSearchResultList);
            this.mUserTab.Controls.Add(this.mSearchUserLabel);
            this.mUserTab.Controls.Add(this.mAddUserButton);
            this.mUserTab.Controls.Add(this.mUserSearchProgress);
            this.mUserTab.Controls.Add(this.mClearUserSearch);
            this.mUserTab.Controls.Add(this.mSearchUserByLabel);
            this.mUserTab.Controls.Add(this.mSearchUser);
            this.mUserTab.Controls.Add(this.mUserSearchOptions);
            this.mUserTab.Controls.Add(this.mUserSearchField);
            this.mUserTab.Location = new System.Drawing.Point(4, 22);
            this.mUserTab.Name = "mUserTab";
            this.mUserTab.Padding = new System.Windows.Forms.Padding(3);
            this.mUserTab.Size = new System.Drawing.Size(620, 380);
            this.mUserTab.TabIndex = 1;
            this.mUserTab.Text = "User";
            // 
            // mUserSearchResultList
            // 
            this.mUserSearchResultList.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mUserSearchResultList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.id,
            this.firstName,
            this.lastName,
            this.dob,
            this.bloodType,
            this.street,
            this.pinCode,
            this.city,
            this.district,
            this.state});
            this.mUserSearchResultList.GridLines = true;
            this.mUserSearchResultList.Location = new System.Drawing.Point(12, 163);
            this.mUserSearchResultList.MultiSelect = false;
            this.mUserSearchResultList.Name = "mUserSearchResultList";
            this.mUserSearchResultList.Size = new System.Drawing.Size(596, 214);
            this.mUserSearchResultList.TabIndex = 11;
            this.mUserSearchResultList.UseCompatibleStateImageBehavior = false;
            this.mUserSearchResultList.View = System.Windows.Forms.View.Details;
            this.mUserSearchResultList.SelectedIndexChanged += new System.EventHandler(this.mSearchUsersResultList_Select);
            // 
            // id
            // 
            this.id.Text = "Id";
            // 
            // firstName
            // 
            this.firstName.Text = "First Name";
            // 
            // lastName
            // 
            this.lastName.Text = "Last Name";
            // 
            // dob
            // 
            this.dob.Text = "DOB";
            // 
            // bloodType
            // 
            this.bloodType.Text = "Blood Type";
            // 
            // street
            // 
            this.street.Text = "Street";
            // 
            // pinCode
            // 
            this.pinCode.Text = "Pin Code";
            // 
            // city
            // 
            this.city.Text = "City";
            // 
            // district
            // 
            this.district.Text = "District";
            // 
            // state
            // 
            this.state.Text = "State";
            // 
            // mSearchUserLabel
            // 
            this.mSearchUserLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mSearchUserLabel.Depth = 0;
            this.mSearchUserLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.mSearchUserLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mSearchUserLabel.Location = new System.Drawing.Point(12, 227);
            this.mSearchUserLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.mSearchUserLabel.Name = "mSearchUserLabel";
            this.mSearchUserLabel.Size = new System.Drawing.Size(596, 22);
            this.mSearchUserLabel.TabIndex = 10;
            // 
            // mAddUserButton
            // 
            this.mAddUserButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mAddUserButton.Depth = 0;
            this.mAddUserButton.Location = new System.Drawing.Point(512, 94);
            this.mAddUserButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.mAddUserButton.Name = "mAddUserButton";
            this.mAddUserButton.Primary = true;
            this.mAddUserButton.Size = new System.Drawing.Size(96, 23);
            this.mAddUserButton.TabIndex = 9;
            this.mAddUserButton.Text = "Add User";
            this.mAddUserButton.UseVisualStyleBackColor = true;
            this.mAddUserButton.Click += new System.EventHandler(this.mAddUserButton_Click);
            // 
            // mUserSearchProgress
            // 
            this.mUserSearchProgress.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mUserSearchProgress.Location = new System.Drawing.Point(12, 123);
            this.mUserSearchProgress.MarqueeAnimationSpeed = 10;
            this.mUserSearchProgress.Name = "mUserSearchProgress";
            this.mUserSearchProgress.Size = new System.Drawing.Size(596, 23);
            this.mUserSearchProgress.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.mUserSearchProgress.TabIndex = 8;
            this.mUserSearchProgress.Visible = false;
            // 
            // mClearUserSearch
            // 
            this.mClearUserSearch.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mClearUserSearch.Depth = 0;
            this.mClearUserSearch.Location = new System.Drawing.Point(127, 94);
            this.mClearUserSearch.MouseState = MaterialSkin.MouseState.HOVER;
            this.mClearUserSearch.Name = "mClearUserSearch";
            this.mClearUserSearch.Primary = true;
            this.mClearUserSearch.Size = new System.Drawing.Size(96, 23);
            this.mClearUserSearch.TabIndex = 6;
            this.mClearUserSearch.Text = "Clear";
            this.mClearUserSearch.UseVisualStyleBackColor = true;
            this.mClearUserSearch.Click += new System.EventHandler(this.mClearUserSearch_Click);
            // 
            // mSearchUserByLabel
            // 
            this.mSearchUserByLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mSearchUserByLabel.AutoSize = true;
            this.mSearchUserByLabel.Depth = 0;
            this.mSearchUserByLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.mSearchUserByLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mSearchUserByLabel.Location = new System.Drawing.Point(8, 57);
            this.mSearchUserByLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.mSearchUserByLabel.Name = "mSearchUserByLabel";
            this.mSearchUserByLabel.Size = new System.Drawing.Size(82, 19);
            this.mSearchUserByLabel.TabIndex = 5;
            this.mSearchUserByLabel.Text = "Search by :";
            // 
            // mSearchUser
            // 
            this.mSearchUser.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mSearchUser.Depth = 0;
            this.mSearchUser.Location = new System.Drawing.Point(12, 94);
            this.mSearchUser.MouseState = MaterialSkin.MouseState.HOVER;
            this.mSearchUser.Name = "mSearchUser";
            this.mSearchUser.Primary = true;
            this.mSearchUser.Size = new System.Drawing.Size(96, 23);
            this.mSearchUser.TabIndex = 4;
            this.mSearchUser.Text = "Search";
            this.mSearchUser.UseVisualStyleBackColor = true;
            this.mSearchUser.Click += new System.EventHandler(this.mSearchUser_Click);
            // 
            // mUserSearchOptions
            // 
            this.mUserSearchOptions.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mUserSearchOptions.FormattingEnabled = true;
            this.mUserSearchOptions.Location = new System.Drawing.Point(96, 55);
            this.mUserSearchOptions.Name = "mUserSearchOptions";
            this.mUserSearchOptions.Size = new System.Drawing.Size(512, 21);
            this.mUserSearchOptions.TabIndex = 3;
            // 
            // mUserSearchField
            // 
            this.mUserSearchField.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mUserSearchField.Depth = 0;
            this.mUserSearchField.Hint = "Enter user name or id";
            this.mUserSearchField.Location = new System.Drawing.Point(12, 16);
            this.mUserSearchField.Margin = new System.Windows.Forms.Padding(16);
            this.mUserSearchField.MouseState = MaterialSkin.MouseState.HOVER;
            this.mUserSearchField.Name = "mUserSearchField";
            this.mUserSearchField.Padding = new System.Windows.Forms.Padding(16);
            this.mUserSearchField.PasswordChar = '\0';
            this.mUserSearchField.SelectedText = "";
            this.mUserSearchField.SelectionLength = 0;
            this.mUserSearchField.SelectionStart = 0;
            this.mUserSearchField.Size = new System.Drawing.Size(596, 23);
            this.mUserSearchField.TabIndex = 0;
            this.mUserSearchField.UseSystemPasswordChar = false;
            // 
            // mBloodBankTab
            // 
            this.mBloodBankTab.BackColor = System.Drawing.SystemColors.Control;
            this.mBloodBankTab.Controls.Add(this.mBloodBankSearch);
            this.mBloodBankTab.Controls.Add(this.mBloodBankName);
            this.mBloodBankTab.Controls.Add(this.mAcquireProgress);
            this.mBloodBankTab.Controls.Add(this.mSearchProgress);
            this.mBloodBankTab.Controls.Add(this.mUserId);
            this.mBloodBankTab.Controls.Add(this.mAcquire);
            this.mBloodBankTab.Controls.Add(this.materialLabel5);
            this.mBloodBankTab.Controls.Add(this.mBloodBankCity);
            this.mBloodBankTab.Controls.Add(this.mBloodVolume);
            this.mBloodBankTab.Controls.Add(this.mBloodBankList);
            this.mBloodBankTab.Controls.Add(this.mBloodBankState);
            this.mBloodBankTab.Controls.Add(this.mBloodBankBloodType);
            this.mBloodBankTab.Controls.Add(this.mSearchByLocation);
            this.mBloodBankTab.Location = new System.Drawing.Point(4, 22);
            this.mBloodBankTab.Name = "mBloodBankTab";
            this.mBloodBankTab.Padding = new System.Windows.Forms.Padding(3);
            this.mBloodBankTab.Size = new System.Drawing.Size(620, 380);
            this.mBloodBankTab.TabIndex = 2;
            this.mBloodBankTab.Text = "Blood Bank";
            // 
            // mBloodBankSearch
            // 
            this.mBloodBankSearch.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mBloodBankSearch.Depth = 0;
            this.mBloodBankSearch.Location = new System.Drawing.Point(501, 59);
            this.mBloodBankSearch.MouseState = MaterialSkin.MouseState.HOVER;
            this.mBloodBankSearch.Name = "mBloodBankSearch";
            this.mBloodBankSearch.Primary = true;
            this.mBloodBankSearch.Size = new System.Drawing.Size(111, 23);
            this.mBloodBankSearch.TabIndex = 12;
            this.mBloodBankSearch.Text = "Search";
            this.mBloodBankSearch.UseVisualStyleBackColor = true;
            this.mBloodBankSearch.Click += new System.EventHandler(this.mBloodBankSearch_Click);
            // 
            // mBloodBankName
            // 
            this.mBloodBankName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mBloodBankName.Depth = 0;
            this.mBloodBankName.Font = new System.Drawing.Font("Roboto", 11F);
            this.mBloodBankName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mBloodBankName.Location = new System.Drawing.Point(8, 262);
            this.mBloodBankName.MouseState = MaterialSkin.MouseState.HOVER;
            this.mBloodBankName.Name = "mBloodBankName";
            this.mBloodBankName.Size = new System.Drawing.Size(604, 27);
            this.mBloodBankName.TabIndex = 11;
            this.mBloodBankName.Text = "(no blood bank selected)";
            // 
            // mAcquireProgress
            // 
            this.mAcquireProgress.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mAcquireProgress.Location = new System.Drawing.Point(318, 349);
            this.mAcquireProgress.MarqueeAnimationSpeed = 10;
            this.mAcquireProgress.Name = "mAcquireProgress";
            this.mAcquireProgress.Size = new System.Drawing.Size(294, 23);
            this.mAcquireProgress.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.mAcquireProgress.TabIndex = 10;
            this.mAcquireProgress.Visible = false;
            // 
            // mSearchProgress
            // 
            this.mSearchProgress.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mSearchProgress.Location = new System.Drawing.Point(165, 59);
            this.mSearchProgress.MarqueeAnimationSpeed = 10;
            this.mSearchProgress.Name = "mSearchProgress";
            this.mSearchProgress.Size = new System.Drawing.Size(330, 23);
            this.mSearchProgress.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.mSearchProgress.TabIndex = 9;
            this.mSearchProgress.Visible = false;
            // 
            // mUserId
            // 
            this.mUserId.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mUserId.Depth = 0;
            this.mUserId.Hint = "User Id";
            this.mUserId.Location = new System.Drawing.Point(318, 311);
            this.mUserId.MouseState = MaterialSkin.MouseState.HOVER;
            this.mUserId.Name = "mUserId";
            this.mUserId.PasswordChar = '\0';
            this.mUserId.SelectedText = "";
            this.mUserId.SelectionLength = 0;
            this.mUserId.SelectionStart = 0;
            this.mUserId.Size = new System.Drawing.Size(294, 23);
            this.mUserId.TabIndex = 8;
            this.mUserId.UseSystemPasswordChar = false;
            // 
            // mAcquire
            // 
            this.mAcquire.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mAcquire.Depth = 0;
            this.mAcquire.Location = new System.Drawing.Point(12, 349);
            this.mAcquire.MouseState = MaterialSkin.MouseState.HOVER;
            this.mAcquire.Name = "mAcquire";
            this.mAcquire.Primary = true;
            this.mAcquire.Size = new System.Drawing.Size(111, 23);
            this.mAcquire.TabIndex = 7;
            this.mAcquire.Text = "Acquire";
            this.mAcquire.UseVisualStyleBackColor = true;
            this.mAcquire.Click += new System.EventHandler(this.mAcquire_Click);
            // 
            // materialLabel5
            // 
            this.materialLabel5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.materialLabel5.Depth = 0;
            this.materialLabel5.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel5.Location = new System.Drawing.Point(8, 13);
            this.materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel5.Name = "materialLabel5";
            this.materialLabel5.Size = new System.Drawing.Size(124, 24);
            this.materialLabel5.TabIndex = 6;
            this.materialLabel5.Text = "Blood Type";
            // 
            // mBloodBankCity
            // 
            this.mBloodBankCity.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mBloodBankCity.Depth = 0;
            this.mBloodBankCity.Enabled = false;
            this.mBloodBankCity.Hint = "City";
            this.mBloodBankCity.Location = new System.Drawing.Point(318, 111);
            this.mBloodBankCity.MouseState = MaterialSkin.MouseState.HOVER;
            this.mBloodBankCity.Name = "mBloodBankCity";
            this.mBloodBankCity.PasswordChar = '\0';
            this.mBloodBankCity.SelectedText = "";
            this.mBloodBankCity.SelectionLength = 0;
            this.mBloodBankCity.SelectionStart = 0;
            this.mBloodBankCity.Size = new System.Drawing.Size(294, 23);
            this.mBloodBankCity.TabIndex = 5;
            this.mBloodBankCity.UseSystemPasswordChar = false;
            // 
            // mBloodVolume
            // 
            this.mBloodVolume.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mBloodVolume.Depth = 0;
            this.mBloodVolume.Hint = "Blood Volume (ml)";
            this.mBloodVolume.Location = new System.Drawing.Point(12, 311);
            this.mBloodVolume.MouseState = MaterialSkin.MouseState.HOVER;
            this.mBloodVolume.Name = "mBloodVolume";
            this.mBloodVolume.PasswordChar = '\0';
            this.mBloodVolume.SelectedText = "";
            this.mBloodVolume.SelectionLength = 0;
            this.mBloodVolume.SelectionStart = 0;
            this.mBloodVolume.Size = new System.Drawing.Size(294, 23);
            this.mBloodVolume.TabIndex = 4;
            this.mBloodVolume.UseSystemPasswordChar = false;
            // 
            // mBloodBankList
            // 
            this.mBloodBankList.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mBloodBankList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.bb_name,
            this.bb_type,
            this.bb_vol,
            this.bb_street,
            this.bb_district,
            this.bb_pincode,
            this.bb_city,
            this.bb_state});
            this.mBloodBankList.Location = new System.Drawing.Point(12, 152);
            this.mBloodBankList.Name = "mBloodBankList";
            this.mBloodBankList.Size = new System.Drawing.Size(600, 97);
            this.mBloodBankList.TabIndex = 3;
            this.mBloodBankList.UseCompatibleStateImageBehavior = false;
            this.mBloodBankList.View = System.Windows.Forms.View.Details;
            this.mBloodBankList.SelectedIndexChanged += new System.EventHandler(this.mBloodBankList_Select);
            // 
            // bb_name
            // 
            this.bb_name.Text = "Blood Bank";
            this.bb_name.Width = 100;
            // 
            // bb_type
            // 
            this.bb_type.Text = "Blood Type";
            this.bb_type.Width = 70;
            // 
            // bb_vol
            // 
            this.bb_vol.DisplayIndex = 7;
            this.bb_vol.Text = "Volume (ml)";
            this.bb_vol.Width = 75;
            // 
            // bb_street
            // 
            this.bb_street.DisplayIndex = 2;
            this.bb_street.Text = "Street";
            this.bb_street.Width = 75;
            // 
            // bb_district
            // 
            this.bb_district.DisplayIndex = 3;
            this.bb_district.Text = "District";
            this.bb_district.Width = 70;
            // 
            // bb_pincode
            // 
            this.bb_pincode.DisplayIndex = 4;
            this.bb_pincode.Text = "Pin Code";
            this.bb_pincode.Width = 70;
            // 
            // bb_city
            // 
            this.bb_city.DisplayIndex = 5;
            this.bb_city.Text = "City";
            this.bb_city.Width = 70;
            // 
            // bb_state
            // 
            this.bb_state.DisplayIndex = 6;
            this.bb_state.Text = "State";
            this.bb_state.Width = 70;
            // 
            // mBloodBankState
            // 
            this.mBloodBankState.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mBloodBankState.Depth = 0;
            this.mBloodBankState.Enabled = false;
            this.mBloodBankState.Hint = "State";
            this.mBloodBankState.Location = new System.Drawing.Point(12, 111);
            this.mBloodBankState.MouseState = MaterialSkin.MouseState.HOVER;
            this.mBloodBankState.Name = "mBloodBankState";
            this.mBloodBankState.PasswordChar = '\0';
            this.mBloodBankState.SelectedText = "";
            this.mBloodBankState.SelectionLength = 0;
            this.mBloodBankState.SelectionStart = 0;
            this.mBloodBankState.Size = new System.Drawing.Size(294, 23);
            this.mBloodBankState.TabIndex = 2;
            this.mBloodBankState.UseSystemPasswordChar = false;
            // 
            // mBloodBankBloodType
            // 
            this.mBloodBankBloodType.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mBloodBankBloodType.FormattingEnabled = true;
            this.mBloodBankBloodType.Location = new System.Drawing.Point(165, 14);
            this.mBloodBankBloodType.Name = "mBloodBankBloodType";
            this.mBloodBankBloodType.Size = new System.Drawing.Size(447, 21);
            this.mBloodBankBloodType.TabIndex = 1;
            // 
            // mSearchByLocation
            // 
            this.mSearchByLocation.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mSearchByLocation.Depth = 0;
            this.mSearchByLocation.Font = new System.Drawing.Font("Roboto", 10F);
            this.mSearchByLocation.Location = new System.Drawing.Point(5, 56);
            this.mSearchByLocation.Margin = new System.Windows.Forms.Padding(0);
            this.mSearchByLocation.MouseLocation = new System.Drawing.Point(-1, -1);
            this.mSearchByLocation.MouseState = MaterialSkin.MouseState.HOVER;
            this.mSearchByLocation.Name = "mSearchByLocation";
            this.mSearchByLocation.Ripple = true;
            this.mSearchByLocation.Size = new System.Drawing.Size(155, 30);
            this.mSearchByLocation.TabIndex = 0;
            this.mSearchByLocation.Text = "Search by location";
            this.mSearchByLocation.UseVisualStyleBackColor = true;
            this.mSearchByLocation.CheckedChanged += new System.EventHandler(this.mSearchByLocation_CheckedChanged);
            // 
            // mSettingsTab
            // 
            this.mSettingsTab.BackColor = System.Drawing.SystemColors.Control;
            this.mSettingsTab.Controls.Add(this.mExitSettings);
            this.mSettingsTab.Controls.Add(this.mChangePassword);
            this.mSettingsTab.Controls.Add(this.mLogOut);
            this.mSettingsTab.Location = new System.Drawing.Point(4, 22);
            this.mSettingsTab.Name = "mSettingsTab";
            this.mSettingsTab.Padding = new System.Windows.Forms.Padding(3);
            this.mSettingsTab.Size = new System.Drawing.Size(620, 380);
            this.mSettingsTab.TabIndex = 3;
            this.mSettingsTab.Text = "Settings";
            // 
            // mExitSettings
            // 
            this.mExitSettings.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mExitSettings.Depth = 0;
            this.mExitSettings.Location = new System.Drawing.Point(217, 231);
            this.mExitSettings.MouseState = MaterialSkin.MouseState.HOVER;
            this.mExitSettings.Name = "mExitSettings";
            this.mExitSettings.Primary = true;
            this.mExitSettings.Size = new System.Drawing.Size(180, 23);
            this.mExitSettings.TabIndex = 2;
            this.mExitSettings.Text = "Exit";
            this.mExitSettings.UseVisualStyleBackColor = true;
            this.mExitSettings.Click += new System.EventHandler(this.mExitSettings_Click);
            // 
            // mChangePassword
            // 
            this.mChangePassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mChangePassword.Depth = 0;
            this.mChangePassword.Location = new System.Drawing.Point(217, 185);
            this.mChangePassword.MouseState = MaterialSkin.MouseState.HOVER;
            this.mChangePassword.Name = "mChangePassword";
            this.mChangePassword.Primary = true;
            this.mChangePassword.Size = new System.Drawing.Size(180, 23);
            this.mChangePassword.TabIndex = 1;
            this.mChangePassword.Text = "Change Password";
            this.mChangePassword.UseVisualStyleBackColor = true;
            this.mChangePassword.Click += new System.EventHandler(this.mChangePassword_Click);
            // 
            // mLogOut
            // 
            this.mLogOut.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mLogOut.Depth = 0;
            this.mLogOut.Location = new System.Drawing.Point(217, 137);
            this.mLogOut.MouseState = MaterialSkin.MouseState.HOVER;
            this.mLogOut.Name = "mLogOut";
            this.mLogOut.Primary = true;
            this.mLogOut.Size = new System.Drawing.Size(180, 23);
            this.mLogOut.TabIndex = 0;
            this.mLogOut.Text = "Log Out";
            this.mLogOut.UseVisualStyleBackColor = true;
            this.mLogOut.Click += new System.EventHandler(this.mLogOut_Click);
            // 
            // mHomeTabSelector
            // 
            this.mHomeTabSelector.BaseTabControl = this.mHomeTabContainer;
            this.mHomeTabSelector.Depth = 0;
            this.mHomeTabSelector.Dock = System.Windows.Forms.DockStyle.Top;
            this.mHomeTabSelector.Location = new System.Drawing.Point(0, 0);
            this.mHomeTabSelector.Margin = new System.Windows.Forms.Padding(0);
            this.mHomeTabSelector.MouseState = MaterialSkin.MouseState.HOVER;
            this.mHomeTabSelector.Name = "mHomeTabSelector";
            this.mHomeTabSelector.Size = new System.Drawing.Size(628, 39);
            this.mHomeTabSelector.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(628, 509);
            this.Controls.Add(this.mHomePanel);
            this.Controls.Add(this.mLoginPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Euphemia | Blood Donation & Retrieval System";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.mLoginPanel.ResumeLayout(false);
            this.mLoginPanel.PerformLayout();
            this.mHomePanel.ResumeLayout(false);
            this.mHomeTabContainer.ResumeLayout(false);
            this.mHomeTab.ResumeLayout(false);
            this.mUserTab.ResumeLayout(false);
            this.mUserTab.PerformLayout();
            this.mBloodBankTab.ResumeLayout(false);
            this.mSettingsTab.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel mLoginPanel;
        private MaterialSkin.Controls.MaterialSingleLineTextField mLoginPassword;
        private MaterialSkin.Controls.MaterialRaisedButton mExitButton;
        private MaterialSkin.Controls.MaterialRaisedButton mLoginButton;
        private MaterialSkin.Controls.MaterialLabel mInvalidPasswordLabel;
        private MaterialSkin.Controls.MaterialLabel mLoginLabel;
        private System.Windows.Forms.Panel mHomePanel;
        private MaterialSkin.Controls.MaterialTabSelector mHomeTabSelector;
        private MaterialSkin.Controls.MaterialTabControl mHomeTabContainer;
        private System.Windows.Forms.TabPage mHomeTab;
        private System.Windows.Forms.TabPage mUserTab;
        private System.Windows.Forms.TabPage mBloodBankTab;
        private System.Windows.Forms.TabPage mSettingsTab;
        private MaterialSkin.Controls.MaterialRaisedButton mLogOut;
        private MaterialSkin.Controls.MaterialRaisedButton mChangePassword;
        private MaterialSkin.Controls.MaterialRaisedButton mExitSettings;
        private MaterialSkin.Controls.MaterialSingleLineTextField mUserSearchField;
        private System.Windows.Forms.ComboBox mUserSearchOptions;
        private MaterialSkin.Controls.MaterialRaisedButton mSearchUser;
        private MaterialSkin.Controls.MaterialLabel mSearchUserByLabel;
        private MaterialSkin.Controls.MaterialRaisedButton mClearUserSearch;
        private System.Windows.Forms.ProgressBar mUserSearchProgress;
        private MaterialSkin.Controls.MaterialRaisedButton mAddUserButton;
        private MaterialSkin.Controls.MaterialLabel mSearchUserLabel;
        private System.Windows.Forms.ListView mUserSearchResultList;
        private System.Windows.Forms.ColumnHeader id;
        private System.Windows.Forms.ColumnHeader firstName;
        private System.Windows.Forms.ColumnHeader lastName;
        private System.Windows.Forms.ColumnHeader dob;
        private System.Windows.Forms.ColumnHeader bloodType;
        private System.Windows.Forms.ColumnHeader street;
        private System.Windows.Forms.ColumnHeader pinCode;
        private System.Windows.Forms.ColumnHeader city;
        private System.Windows.Forms.ColumnHeader district;
        private System.Windows.Forms.ColumnHeader state;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private System.Windows.Forms.ListView mBloodBankList;
        private MaterialSkin.Controls.MaterialSingleLineTextField mBloodBankState;
        private System.Windows.Forms.ComboBox mBloodBankBloodType;
        private MaterialSkin.Controls.MaterialCheckBox mSearchByLocation;
        private MaterialSkin.Controls.MaterialSingleLineTextField mBloodBankCity;
        private MaterialSkin.Controls.MaterialSingleLineTextField mBloodVolume;
        private MaterialSkin.Controls.MaterialLabel materialLabel5;
        private MaterialSkin.Controls.MaterialRaisedButton mAcquire;
        private System.Windows.Forms.ProgressBar mSearchProgress;
        private MaterialSkin.Controls.MaterialSingleLineTextField mUserId;
        private System.Windows.Forms.ProgressBar mAcquireProgress;
        private MaterialSkin.Controls.MaterialLabel mBloodBankName;
        private System.Windows.Forms.ColumnHeader bb_name;
        private System.Windows.Forms.ColumnHeader bb_type;
        private System.Windows.Forms.ColumnHeader bb_street;
        private System.Windows.Forms.ColumnHeader bb_district;
        private System.Windows.Forms.ColumnHeader bb_pincode;
        private System.Windows.Forms.ColumnHeader bb_city;
        private System.Windows.Forms.ColumnHeader bb_state;
        private System.Windows.Forms.ColumnHeader bb_vol;
        private MaterialSkin.Controls.MaterialRaisedButton mBloodBankSearch;
    }
}

