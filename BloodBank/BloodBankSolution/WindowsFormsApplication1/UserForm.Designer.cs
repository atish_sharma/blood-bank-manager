﻿namespace BloodBank
{
    partial class UserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserForm));
            this.mUserTabSelector = new MaterialSkin.Controls.MaterialTabSelector();
            this.mUserTabControl = new MaterialSkin.Controls.MaterialTabControl();
            this.mInfoTab = new System.Windows.Forms.TabPage();
            this.mId = new MaterialSkin.Controls.MaterialLabel();
            this.materialDivider4 = new MaterialSkin.Controls.MaterialDivider();
            this.mIdHeader = new System.Windows.Forms.Label();
            this.mFirstName = new MaterialSkin.Controls.MaterialLabel();
            this.materialDivider3 = new MaterialSkin.Controls.MaterialDivider();
            this.mPersonalInfoHeader = new System.Windows.Forms.Label();
            this.mEmailId = new MaterialSkin.Controls.MaterialLabel();
            this.materialDivider2 = new MaterialSkin.Controls.MaterialDivider();
            this.mEmail = new System.Windows.Forms.Label();
            this.mPhone = new MaterialSkin.Controls.MaterialLabel();
            this.materialDivider1 = new MaterialSkin.Controls.MaterialDivider();
            this.mPhoneNumberHeader = new System.Windows.Forms.Label();
            this.mAddress = new MaterialSkin.Controls.MaterialLabel();
            this.mAddressLabel = new System.Windows.Forms.Label();
            this.mAddressDivider = new MaterialSkin.Controls.MaterialDivider();
            this.mDob = new MaterialSkin.Controls.MaterialLabel();
            this.mBloodType = new MaterialSkin.Controls.MaterialLabel();
            this.mLastName = new MaterialSkin.Controls.MaterialLabel();
            this.mDonateTab = new System.Windows.Forms.TabPage();
            this.mDonationProgress = new System.Windows.Forms.ProgressBar();
            this.mDonate = new MaterialSkin.Controls.MaterialRaisedButton();
            this.mBloodVolumeLabel = new MaterialSkin.Controls.MaterialLabel();
            this.mBloodBankLabel = new MaterialSkin.Controls.MaterialLabel();
            this.mBloodVolume = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.mBloodBankCombo = new System.Windows.Forms.ComboBox();
            this.mLogTab = new System.Windows.Forms.TabPage();
            this.mActionLabel = new MaterialSkin.Controls.MaterialLabel();
            this.mLogList = new System.Windows.Forms.ListView();
            this.action = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.datetime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.blodbank = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.volume = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.timestamp = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mLogProgress = new System.Windows.Forms.ProgressBar();
            this.mClearLog = new MaterialSkin.Controls.MaterialRaisedButton();
            this.mSearchLog = new MaterialSkin.Controls.MaterialRaisedButton();
            this.mLogCombo = new System.Windows.Forms.ComboBox();
            this.mUserTabControl.SuspendLayout();
            this.mInfoTab.SuspendLayout();
            this.mDonateTab.SuspendLayout();
            this.mLogTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // mUserTabSelector
            // 
            this.mUserTabSelector.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mUserTabSelector.BaseTabControl = this.mUserTabControl;
            this.mUserTabSelector.Depth = 0;
            this.mUserTabSelector.Location = new System.Drawing.Point(0, 62);
            this.mUserTabSelector.Margin = new System.Windows.Forms.Padding(0);
            this.mUserTabSelector.MouseState = MaterialSkin.MouseState.HOVER;
            this.mUserTabSelector.Name = "mUserTabSelector";
            this.mUserTabSelector.Size = new System.Drawing.Size(629, 42);
            this.mUserTabSelector.TabIndex = 0;
            // 
            // mUserTabControl
            // 
            this.mUserTabControl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mUserTabControl.Controls.Add(this.mInfoTab);
            this.mUserTabControl.Controls.Add(this.mDonateTab);
            this.mUserTabControl.Controls.Add(this.mLogTab);
            this.mUserTabControl.Depth = 0;
            this.mUserTabControl.Location = new System.Drawing.Point(0, 107);
            this.mUserTabControl.MouseState = MaterialSkin.MouseState.HOVER;
            this.mUserTabControl.Name = "mUserTabControl";
            this.mUserTabControl.SelectedIndex = 0;
            this.mUserTabControl.Size = new System.Drawing.Size(628, 403);
            this.mUserTabControl.TabIndex = 1;
            // 
            // mInfoTab
            // 
            this.mInfoTab.BackColor = System.Drawing.SystemColors.Control;
            this.mInfoTab.Controls.Add(this.mId);
            this.mInfoTab.Controls.Add(this.materialDivider4);
            this.mInfoTab.Controls.Add(this.mIdHeader);
            this.mInfoTab.Controls.Add(this.mFirstName);
            this.mInfoTab.Controls.Add(this.materialDivider3);
            this.mInfoTab.Controls.Add(this.mPersonalInfoHeader);
            this.mInfoTab.Controls.Add(this.mEmailId);
            this.mInfoTab.Controls.Add(this.materialDivider2);
            this.mInfoTab.Controls.Add(this.mEmail);
            this.mInfoTab.Controls.Add(this.mPhone);
            this.mInfoTab.Controls.Add(this.materialDivider1);
            this.mInfoTab.Controls.Add(this.mPhoneNumberHeader);
            this.mInfoTab.Controls.Add(this.mAddress);
            this.mInfoTab.Controls.Add(this.mAddressLabel);
            this.mInfoTab.Controls.Add(this.mAddressDivider);
            this.mInfoTab.Controls.Add(this.mDob);
            this.mInfoTab.Controls.Add(this.mBloodType);
            this.mInfoTab.Controls.Add(this.mLastName);
            this.mInfoTab.Location = new System.Drawing.Point(4, 22);
            this.mInfoTab.Name = "mInfoTab";
            this.mInfoTab.Padding = new System.Windows.Forms.Padding(3);
            this.mInfoTab.Size = new System.Drawing.Size(620, 377);
            this.mInfoTab.TabIndex = 0;
            this.mInfoTab.Text = "Information";
            // 
            // mId
            // 
            this.mId.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mId.Depth = 0;
            this.mId.Font = new System.Drawing.Font("Roboto", 11F);
            this.mId.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mId.Location = new System.Drawing.Point(18, 33);
            this.mId.MouseState = MaterialSkin.MouseState.HOVER;
            this.mId.Name = "mId";
            this.mId.Size = new System.Drawing.Size(591, 19);
            this.mId.TabIndex = 17;
            // 
            // materialDivider4
            // 
            this.materialDivider4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.materialDivider4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialDivider4.Depth = 0;
            this.materialDivider4.Location = new System.Drawing.Point(6, 29);
            this.materialDivider4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider4.Name = "materialDivider4";
            this.materialDivider4.Size = new System.Drawing.Size(604, 1);
            this.materialDivider4.TabIndex = 16;
            // 
            // mIdHeader
            // 
            this.mIdHeader.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mIdHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mIdHeader.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.mIdHeader.Location = new System.Drawing.Point(18, 3);
            this.mIdHeader.Name = "mIdHeader";
            this.mIdHeader.Size = new System.Drawing.Size(591, 23);
            this.mIdHeader.TabIndex = 15;
            this.mIdHeader.Text = "ID";
            // 
            // mFirstName
            // 
            this.mFirstName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mFirstName.Depth = 0;
            this.mFirstName.Font = new System.Drawing.Font("Roboto", 11F);
            this.mFirstName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mFirstName.Location = new System.Drawing.Point(18, 85);
            this.mFirstName.MouseState = MaterialSkin.MouseState.HOVER;
            this.mFirstName.Name = "mFirstName";
            this.mFirstName.Size = new System.Drawing.Size(591, 19);
            this.mFirstName.TabIndex = 14;
            // 
            // materialDivider3
            // 
            this.materialDivider3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.materialDivider3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialDivider3.Depth = 0;
            this.materialDivider3.Location = new System.Drawing.Point(6, 81);
            this.materialDivider3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider3.Name = "materialDivider3";
            this.materialDivider3.Size = new System.Drawing.Size(604, 1);
            this.materialDivider3.TabIndex = 13;
            // 
            // mPersonalInfoHeader
            // 
            this.mPersonalInfoHeader.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mPersonalInfoHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mPersonalInfoHeader.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.mPersonalInfoHeader.Location = new System.Drawing.Point(18, 55);
            this.mPersonalInfoHeader.Name = "mPersonalInfoHeader";
            this.mPersonalInfoHeader.Size = new System.Drawing.Size(591, 23);
            this.mPersonalInfoHeader.TabIndex = 12;
            this.mPersonalInfoHeader.Text = "Personal Information";
            // 
            // mEmailId
            // 
            this.mEmailId.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mEmailId.Depth = 0;
            this.mEmailId.Font = new System.Drawing.Font("Roboto", 11F);
            this.mEmailId.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mEmailId.Location = new System.Drawing.Point(18, 346);
            this.mEmailId.MouseState = MaterialSkin.MouseState.HOVER;
            this.mEmailId.Name = "mEmailId";
            this.mEmailId.Size = new System.Drawing.Size(591, 19);
            this.mEmailId.TabIndex = 11;
            // 
            // materialDivider2
            // 
            this.materialDivider2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.materialDivider2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialDivider2.Depth = 0;
            this.materialDivider2.Location = new System.Drawing.Point(5, 342);
            this.materialDivider2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider2.Name = "materialDivider2";
            this.materialDivider2.Size = new System.Drawing.Size(604, 1);
            this.materialDivider2.TabIndex = 10;
            // 
            // mEmail
            // 
            this.mEmail.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mEmail.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.mEmail.Location = new System.Drawing.Point(18, 316);
            this.mEmail.Name = "mEmail";
            this.mEmail.Size = new System.Drawing.Size(591, 23);
            this.mEmail.TabIndex = 9;
            this.mEmail.Text = "Email ID";
            // 
            // mPhone
            // 
            this.mPhone.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mPhone.Depth = 0;
            this.mPhone.Font = new System.Drawing.Font("Roboto", 11F);
            this.mPhone.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mPhone.Location = new System.Drawing.Point(18, 288);
            this.mPhone.MouseState = MaterialSkin.MouseState.HOVER;
            this.mPhone.Name = "mPhone";
            this.mPhone.Size = new System.Drawing.Size(591, 19);
            this.mPhone.TabIndex = 8;
            // 
            // materialDivider1
            // 
            this.materialDivider1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.materialDivider1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialDivider1.Depth = 0;
            this.materialDivider1.Location = new System.Drawing.Point(5, 284);
            this.materialDivider1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider1.Name = "materialDivider1";
            this.materialDivider1.Size = new System.Drawing.Size(604, 1);
            this.materialDivider1.TabIndex = 7;
            // 
            // mPhoneNumberHeader
            // 
            this.mPhoneNumberHeader.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mPhoneNumberHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mPhoneNumberHeader.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.mPhoneNumberHeader.Location = new System.Drawing.Point(18, 258);
            this.mPhoneNumberHeader.Name = "mPhoneNumberHeader";
            this.mPhoneNumberHeader.Size = new System.Drawing.Size(591, 23);
            this.mPhoneNumberHeader.TabIndex = 6;
            this.mPhoneNumberHeader.Text = "Phone Number";
            // 
            // mAddress
            // 
            this.mAddress.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mAddress.Depth = 0;
            this.mAddress.Font = new System.Drawing.Font("Roboto", 11F);
            this.mAddress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mAddress.Location = new System.Drawing.Point(18, 228);
            this.mAddress.MouseState = MaterialSkin.MouseState.HOVER;
            this.mAddress.Name = "mAddress";
            this.mAddress.Size = new System.Drawing.Size(591, 19);
            this.mAddress.TabIndex = 5;
            // 
            // mAddressLabel
            // 
            this.mAddressLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mAddressLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mAddressLabel.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.mAddressLabel.Location = new System.Drawing.Point(18, 198);
            this.mAddressLabel.Name = "mAddressLabel";
            this.mAddressLabel.Size = new System.Drawing.Size(591, 23);
            this.mAddressLabel.TabIndex = 4;
            this.mAddressLabel.Text = "Address";
            // 
            // mAddressDivider
            // 
            this.mAddressDivider.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mAddressDivider.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mAddressDivider.Depth = 0;
            this.mAddressDivider.Location = new System.Drawing.Point(5, 224);
            this.mAddressDivider.MouseState = MaterialSkin.MouseState.HOVER;
            this.mAddressDivider.Name = "mAddressDivider";
            this.mAddressDivider.Size = new System.Drawing.Size(604, 1);
            this.mAddressDivider.TabIndex = 3;
            // 
            // mDob
            // 
            this.mDob.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mDob.Depth = 0;
            this.mDob.Font = new System.Drawing.Font("Roboto", 11F);
            this.mDob.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mDob.Location = new System.Drawing.Point(18, 170);
            this.mDob.MouseState = MaterialSkin.MouseState.HOVER;
            this.mDob.Name = "mDob";
            this.mDob.Size = new System.Drawing.Size(591, 19);
            this.mDob.TabIndex = 2;
            // 
            // mBloodType
            // 
            this.mBloodType.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mBloodType.Depth = 0;
            this.mBloodType.Font = new System.Drawing.Font("Roboto", 11F);
            this.mBloodType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mBloodType.Location = new System.Drawing.Point(18, 142);
            this.mBloodType.MouseState = MaterialSkin.MouseState.HOVER;
            this.mBloodType.Name = "mBloodType";
            this.mBloodType.Size = new System.Drawing.Size(591, 19);
            this.mBloodType.TabIndex = 1;
            // 
            // mLastName
            // 
            this.mLastName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mLastName.Depth = 0;
            this.mLastName.Font = new System.Drawing.Font("Roboto", 11F);
            this.mLastName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mLastName.Location = new System.Drawing.Point(18, 113);
            this.mLastName.MouseState = MaterialSkin.MouseState.HOVER;
            this.mLastName.Name = "mLastName";
            this.mLastName.Size = new System.Drawing.Size(591, 19);
            this.mLastName.TabIndex = 0;
            // 
            // mDonateTab
            // 
            this.mDonateTab.BackColor = System.Drawing.SystemColors.Control;
            this.mDonateTab.Controls.Add(this.mDonationProgress);
            this.mDonateTab.Controls.Add(this.mDonate);
            this.mDonateTab.Controls.Add(this.mBloodVolumeLabel);
            this.mDonateTab.Controls.Add(this.mBloodBankLabel);
            this.mDonateTab.Controls.Add(this.mBloodVolume);
            this.mDonateTab.Controls.Add(this.mBloodBankCombo);
            this.mDonateTab.Location = new System.Drawing.Point(4, 22);
            this.mDonateTab.Name = "mDonateTab";
            this.mDonateTab.Padding = new System.Windows.Forms.Padding(3);
            this.mDonateTab.Size = new System.Drawing.Size(620, 377);
            this.mDonateTab.TabIndex = 1;
            this.mDonateTab.Text = "Donate";
            // 
            // mDonationProgress
            // 
            this.mDonationProgress.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mDonationProgress.Location = new System.Drawing.Point(18, 171);
            this.mDonationProgress.MarqueeAnimationSpeed = 10;
            this.mDonationProgress.Name = "mDonationProgress";
            this.mDonationProgress.Size = new System.Drawing.Size(594, 23);
            this.mDonationProgress.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.mDonationProgress.TabIndex = 6;
            this.mDonationProgress.Visible = false;
            // 
            // mDonate
            // 
            this.mDonate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mDonate.Depth = 0;
            this.mDonate.Location = new System.Drawing.Point(18, 200);
            this.mDonate.MouseState = MaterialSkin.MouseState.HOVER;
            this.mDonate.Name = "mDonate";
            this.mDonate.Primary = true;
            this.mDonate.Size = new System.Drawing.Size(91, 23);
            this.mDonate.TabIndex = 5;
            this.mDonate.Text = "Donate";
            this.mDonate.UseVisualStyleBackColor = true;
            this.mDonate.Click += new System.EventHandler(this.mDonate_Click);
            // 
            // mBloodVolumeLabel
            // 
            this.mBloodVolumeLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mBloodVolumeLabel.Depth = 0;
            this.mBloodVolumeLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.mBloodVolumeLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mBloodVolumeLabel.Location = new System.Drawing.Point(14, 104);
            this.mBloodVolumeLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.mBloodVolumeLabel.Name = "mBloodVolumeLabel";
            this.mBloodVolumeLabel.Size = new System.Drawing.Size(598, 24);
            this.mBloodVolumeLabel.TabIndex = 4;
            this.mBloodVolumeLabel.Text = "How much blood (ml) are you donating?";
            // 
            // mBloodBankLabel
            // 
            this.mBloodBankLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mBloodBankLabel.Depth = 0;
            this.mBloodBankLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.mBloodBankLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mBloodBankLabel.Location = new System.Drawing.Point(14, 18);
            this.mBloodBankLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.mBloodBankLabel.Name = "mBloodBankLabel";
            this.mBloodBankLabel.Size = new System.Drawing.Size(598, 28);
            this.mBloodBankLabel.TabIndex = 3;
            this.mBloodBankLabel.Text = "Which blood bank are you donating to?";
            // 
            // mBloodVolume
            // 
            this.mBloodVolume.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mBloodVolume.Depth = 0;
            this.mBloodVolume.Hint = "Enter volume in mililiters";
            this.mBloodVolume.Location = new System.Drawing.Point(18, 142);
            this.mBloodVolume.MouseState = MaterialSkin.MouseState.HOVER;
            this.mBloodVolume.Name = "mBloodVolume";
            this.mBloodVolume.PasswordChar = '\0';
            this.mBloodVolume.SelectedText = "";
            this.mBloodVolume.SelectionLength = 0;
            this.mBloodVolume.SelectionStart = 0;
            this.mBloodVolume.Size = new System.Drawing.Size(594, 23);
            this.mBloodVolume.TabIndex = 1;
            this.mBloodVolume.UseSystemPasswordChar = false;
            // 
            // mBloodBankCombo
            // 
            this.mBloodBankCombo.FormattingEnabled = true;
            this.mBloodBankCombo.Location = new System.Drawing.Point(18, 70);
            this.mBloodBankCombo.Name = "mBloodBankCombo";
            this.mBloodBankCombo.Size = new System.Drawing.Size(594, 21);
            this.mBloodBankCombo.TabIndex = 0;
            // 
            // mLogTab
            // 
            this.mLogTab.BackColor = System.Drawing.SystemColors.Control;
            this.mLogTab.Controls.Add(this.mActionLabel);
            this.mLogTab.Controls.Add(this.mLogList);
            this.mLogTab.Controls.Add(this.mLogProgress);
            this.mLogTab.Controls.Add(this.mClearLog);
            this.mLogTab.Controls.Add(this.mSearchLog);
            this.mLogTab.Controls.Add(this.mLogCombo);
            this.mLogTab.Location = new System.Drawing.Point(4, 22);
            this.mLogTab.Name = "mLogTab";
            this.mLogTab.Padding = new System.Windows.Forms.Padding(3);
            this.mLogTab.Size = new System.Drawing.Size(620, 377);
            this.mLogTab.TabIndex = 2;
            this.mLogTab.Text = "Log";
            // 
            // mActionLabel
            // 
            this.mActionLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mActionLabel.Depth = 0;
            this.mActionLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.mActionLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mActionLabel.Location = new System.Drawing.Point(8, 18);
            this.mActionLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.mActionLabel.Name = "mActionLabel";
            this.mActionLabel.Size = new System.Drawing.Size(75, 31);
            this.mActionLabel.TabIndex = 5;
            this.mActionLabel.Text = "Action :";
            // 
            // mLogList
            // 
            this.mLogList.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mLogList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.action,
            this.datetime,
            this.blodbank,
            this.volume,
            this.timestamp});
            this.mLogList.GridLines = true;
            this.mLogList.Location = new System.Drawing.Point(8, 95);
            this.mLogList.MultiSelect = false;
            this.mLogList.Name = "mLogList";
            this.mLogList.Size = new System.Drawing.Size(604, 273);
            this.mLogList.TabIndex = 4;
            this.mLogList.UseCompatibleStateImageBehavior = false;
            this.mLogList.View = System.Windows.Forms.View.Details;
            // 
            // action
            // 
            this.action.Text = "Action";
            this.action.Width = 120;
            // 
            // datetime
            // 
            this.datetime.Text = "Date & Time";
            this.datetime.Width = 120;
            // 
            // blodbank
            // 
            this.blodbank.Text = "Blood Bank";
            this.blodbank.Width = 120;
            // 
            // volume
            // 
            this.volume.Text = "Volume (ml)";
            this.volume.Width = 120;
            // 
            // timestamp
            // 
            this.timestamp.Text = "Timestamp";
            this.timestamp.Width = 120;
            // 
            // mLogProgress
            // 
            this.mLogProgress.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mLogProgress.Location = new System.Drawing.Point(98, 54);
            this.mLogProgress.MarqueeAnimationSpeed = 10;
            this.mLogProgress.Name = "mLogProgress";
            this.mLogProgress.Size = new System.Drawing.Size(424, 23);
            this.mLogProgress.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.mLogProgress.TabIndex = 3;
            this.mLogProgress.Visible = false;
            // 
            // mClearLog
            // 
            this.mClearLog.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mClearLog.Depth = 0;
            this.mClearLog.Location = new System.Drawing.Point(528, 54);
            this.mClearLog.MouseState = MaterialSkin.MouseState.HOVER;
            this.mClearLog.Name = "mClearLog";
            this.mClearLog.Primary = true;
            this.mClearLog.Size = new System.Drawing.Size(84, 23);
            this.mClearLog.TabIndex = 2;
            this.mClearLog.Text = "Clear";
            this.mClearLog.UseVisualStyleBackColor = true;
            this.mClearLog.Click += new System.EventHandler(this.mClearLog_Click);
            // 
            // mSearchLog
            // 
            this.mSearchLog.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mSearchLog.Depth = 0;
            this.mSearchLog.Location = new System.Drawing.Point(8, 54);
            this.mSearchLog.MouseState = MaterialSkin.MouseState.HOVER;
            this.mSearchLog.Name = "mSearchLog";
            this.mSearchLog.Primary = true;
            this.mSearchLog.Size = new System.Drawing.Size(84, 23);
            this.mSearchLog.TabIndex = 1;
            this.mSearchLog.Text = "Refresh";
            this.mSearchLog.UseVisualStyleBackColor = true;
            this.mSearchLog.Click += new System.EventHandler(this.mSearchLog_Click);
            // 
            // mLogCombo
            // 
            this.mLogCombo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mLogCombo.FormattingEnabled = true;
            this.mLogCombo.Location = new System.Drawing.Point(98, 18);
            this.mLogCombo.Name = "mLogCombo";
            this.mLogCombo.Size = new System.Drawing.Size(514, 21);
            this.mLogCombo.TabIndex = 0;
            this.mLogCombo.SelectedIndexChanged += new System.EventHandler(this.mLogCombo_SelectedIndexChanged);
            // 
            // UserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(628, 509);
            this.Controls.Add(this.mUserTabControl);
            this.Controls.Add(this.mUserTabSelector);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UserForm";
            this.Text = "User";
            this.mUserTabControl.ResumeLayout(false);
            this.mInfoTab.ResumeLayout(false);
            this.mDonateTab.ResumeLayout(false);
            this.mLogTab.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MaterialSkin.Controls.MaterialTabSelector mUserTabSelector;
        private MaterialSkin.Controls.MaterialTabControl mUserTabControl;
        private System.Windows.Forms.TabPage mInfoTab;
        private System.Windows.Forms.TabPage mDonateTab;
        private System.Windows.Forms.TabPage mLogTab;
        private MaterialSkin.Controls.MaterialLabel mLastName;
        private MaterialSkin.Controls.MaterialLabel mBloodType;
        private MaterialSkin.Controls.MaterialDivider mAddressDivider;
        private MaterialSkin.Controls.MaterialLabel mDob;
        private System.Windows.Forms.Label mAddressLabel;
        private MaterialSkin.Controls.MaterialLabel mAddress;
        private MaterialSkin.Controls.MaterialDivider materialDivider1;
        private System.Windows.Forms.Label mPhoneNumberHeader;
        private MaterialSkin.Controls.MaterialLabel mPhone;
        private MaterialSkin.Controls.MaterialLabel mEmailId;
        private MaterialSkin.Controls.MaterialDivider materialDivider2;
        private System.Windows.Forms.Label mEmail;
        private MaterialSkin.Controls.MaterialLabel mFirstName;
        private MaterialSkin.Controls.MaterialDivider materialDivider3;
        private System.Windows.Forms.Label mPersonalInfoHeader;
        private MaterialSkin.Controls.MaterialLabel mId;
        private MaterialSkin.Controls.MaterialDivider materialDivider4;
        private System.Windows.Forms.Label mIdHeader;
        private System.Windows.Forms.ComboBox mBloodBankCombo;
        private MaterialSkin.Controls.MaterialRaisedButton mDonate;
        private MaterialSkin.Controls.MaterialLabel mBloodVolumeLabel;
        private MaterialSkin.Controls.MaterialLabel mBloodBankLabel;
        private MaterialSkin.Controls.MaterialSingleLineTextField mBloodVolume;
        private System.Windows.Forms.ProgressBar mDonationProgress;
        private System.Windows.Forms.ProgressBar mLogProgress;
        private MaterialSkin.Controls.MaterialRaisedButton mClearLog;
        private MaterialSkin.Controls.MaterialRaisedButton mSearchLog;
        private System.Windows.Forms.ComboBox mLogCombo;
        private System.Windows.Forms.ListView mLogList;
        private System.Windows.Forms.ColumnHeader action;
        private System.Windows.Forms.ColumnHeader datetime;
        private System.Windows.Forms.ColumnHeader blodbank;
        private System.Windows.Forms.ColumnHeader volume;
        private System.Windows.Forms.ColumnHeader timestamp;
        private MaterialSkin.Controls.MaterialLabel mActionLabel;
    }
}