﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloodBank
{
    class User
    {

        public long Id;
        public string FirstName;
        public string LastName;
        public string Dob;
        public string BloodType;
        public string Street;
        public string PinCode;
        public string City;
        public string District;
        public string State;
        public ArrayList phone;
        public ArrayList email;

        public User()
        {

        }

        public User(long id,string firstName,string lastName,string dob,string bloodType,string street,string pinCode,string city,string district,string state)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Dob = dob;
            BloodType = bloodType;
            Street = street;
            PinCode = pinCode;
            City = city;
            District = district;
            State = state;
        }

    }
}
