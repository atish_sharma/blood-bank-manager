﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Animations;
using MaterialSkin.Controls;
using System.Threading;
using MySql.Data.MySqlClient;

namespace BloodBank
{
    public partial class UserForm : MaterialForm
    {

        public static string ACTION_DONATE = "DONATE";
        public static string ACTION_RECEIVE = "RECEIVE";
        private AsyncResultManager mAsyncResultManager;
        private DatabaseManager mDatabaseManager;
        private string[] mLogStatus = {"Donated/Received","Donated","Recieved"};
        private User mUser;
        private Boolean donationSuccess;
        private Boolean logRefreshSuccess;
        private ArrayList bloodTransactions = new ArrayList();

        public UserForm(String id)
        {
            InitializeComponent();
            mDatabaseManager = DatabaseManager.getInstance();
            mAsyncResultManager = AsyncResultManager.getInstance();
            DesignManager.setDesign(this);
            long Id=Int64.Parse(id);
            foreach(User user in mAsyncResultManager.mSearchUsersQueryResult)
            {
                if(user.Id==Id)
                {
                    mUser = user;
                    break;
                }
            }
            this.Text = cstring(mUser.FirstName + " " + mUser.LastName);
            mId.Text = mUser.Id.ToString();
            mFirstName.Text = "First Name : "+cstring(mUser.FirstName);
            mLastName.Text = "Last Name : " + cstring(mUser.LastName);
            mDob.Text = "Date of Birth : " + cstring(mUser.Dob);
            mBloodType.Text = "Blood Type : " + cstring(mUser.BloodType);
            mAddress.Text = cstring(mUser.Street + ", " + mUser.District + ", " + mUser.PinCode + ", " + mUser.City + ", " + mUser.State);
            mPhone.Text = mEmailId.Text = "";
            foreach(string s in mUser.phone)
            {
                mPhone.Text += s + " ";
            }
            foreach (string s in mUser.email)
            {
                mEmailId.Text += s + " ";
            }
            string[] bloodBankTitles=new string[mAsyncResultManager.mBloodBanks.Count];
            int i=0;
            foreach(BloodBank bank in mAsyncResultManager.mBloodBanks)
            {
                bloodBankTitles[i++] = String.Format("{0} [{1},{2},{3},{4},{5}]", bank.VaultName, bank.Street, bank.District, bank.PinCode, bank.City, bank.State);
            }
            mBloodBankCombo.Items.AddRange(bloodBankTitles);
            mLogCombo.Items.AddRange(mLogStatus);
            try 
            {
                mBloodBankCombo.SelectedIndex = 0;
                mLogCombo.SelectedIndex = 0;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public static string cstring(string s)
        {
            return Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(s);
        }

        public static string UnixTimeStampToDateTime(double unixTimeStamp)
        {
            System.DateTime dateTime = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
            dateTime = dateTime.AddSeconds(unixTimeStamp+(3600*5.5));
            string date = dateTime.ToShortDateString() + " " + dateTime.ToShortTimeString();
            return date;
        }

        private async void mDonate_Click(object sender, EventArgs e)
        {
            string bloodBank = mBloodBankCombo.SelectedItem.ToString();
            string volume = mBloodVolume.Text.Trim();
            Boolean error = false;
            string err = "";
            if(bloodBank.Equals(""))
            {
                error = true;
                err += "Please select the blood bank you want to donate to.\n";
            }
            long n;
            if(volume.Equals(""))
            {
                error = true;
                err += "Please enter volume.\n";
            }
            else if(!Int64.TryParse(volume,out n))
            {
                error = true;
                err += "Please enter a number as volume.\n";
            }
            if(error)
            {
                DesignManager.message("Error", err, MessageBoxIcon.Error);
                return;
            }
            donationSuccess = false;
            long vol = Int64.Parse(volume);
            string bloodType = mUser.BloodType;
            long bloodBankId = ((BloodBank)mAsyncResultManager.mBloodBanks[mBloodBankCombo.SelectedIndex]).Id;
            long userId = mUser.Id;
            mDonationProgress.Visible = true;
            await Task.Factory.StartNew(() => donate(userId,bloodBankId,vol,bloodType), TaskCreationOptions.LongRunning);
            mDonationProgress.Visible = false;
            if(donationSuccess)
            {
                mBloodVolume.Text = "";
                DesignManager.message("Donation Successful", "Blood donation successful.", MessageBoxIcon.Information);
            }
            else
            {
                DesignManager.message("Error", "Unable to query database! Please check your internet connection.", MessageBoxIcon.Error);
            }
        }

        private void donate(long userId,long bloodBankId,long volume,string bloodType)
        {
            MySqlConnection conn = mDatabaseManager.getConnection();
            if (conn == null)
            {
                return;
            }
            long timestamp = (long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            try
            {
                string stm = String.Format("insert into donor (id,volume_donated,timestamp_donated,bank_id) values ({0},{1},{2},{3})", userId, volume, timestamp,bloodBankId);
                MySqlCommand cmd = new MySqlCommand(stm, conn);
                cmd.ExecuteNonQuery();
                stm = String.Format("update bloodvault set blood_volume=blood_volume+{0} where bank_id={1} and blood_type='{2}'", volume, bloodBankId, bloodType);
                cmd = new MySqlCommand(stm, conn);
                cmd.ExecuteNonQuery();
                donationSuccess = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                donationSuccess = false;
            }
            mDatabaseManager.closeConnection();
        }

        private async void mSearchLog_Click(object sender, EventArgs e)
        {
            int action = mLogCombo.SelectedIndex;
            long userId = mUser.Id;
            logRefreshSuccess = false;
            mLogProgress.Visible = true;
            await Task.Factory.StartNew(() => refreshLog(action,userId), TaskCreationOptions.LongRunning);
            mLogProgress.Visible = false;
            if(logRefreshSuccess)
            {
                mLogList.Items.Clear();
                if (bloodTransactions.Count == 0)
                {
                    DesignManager.message("Uh Oh...", "It looks like you have an empty log.", MessageBoxIcon.Information);
                    return;
                }
                populateLogList();
            }
            else
            {
                DesignManager.message("Error", "Unable to query database! Please check your internet connection.", MessageBoxIcon.Error);
            }
        }

        private void populateLogList()
        {
            foreach (string s in bloodTransactions)
            {
                string[] str = s.Split(';');
                string action = str[3];
                if(mLogCombo.SelectedIndex==1 && !action.Equals(ACTION_DONATE))
                {
                    continue;
                }
                if (mLogCombo.SelectedIndex == 2 && !action.Equals(ACTION_RECEIVE))
                {
                    continue;
                }
                string volume = str[1];
                string timestamp = str[0];
                string name = str[2];
                string datetime = UnixTimeStampToDateTime(Int64.Parse(timestamp));
                ListViewItem item = new ListViewItem(str[3]);
                item.SubItems.Add(datetime);
                item.SubItems.Add(cstring(name));
                item.SubItems.Add(volume);
                item.SubItems.Add(timestamp);
                mLogList.Items.Add(item);
            }
        }

        private void refreshLog(int action,long id)
        {
            MySqlConnection conn = mDatabaseManager.getConnection();
            if (conn == null)
            {
                return;
            }
            try
            {
                bloodTransactions = new ArrayList();
                string stm = String.Format("select timestamp_donated,volume_donated,vault_name from bankvault natural join donor where donor.id={0} order by timestamp_donated desc", id);
                MySqlCommand cmd = new MySqlCommand(stm, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                while(rdr.Read())
                {
                    bloodTransactions.Add(String.Format("{0};{1};{2};{3}",rdr.GetString(0),rdr.GetString(1),rdr.GetString(2),ACTION_DONATE));
                }
                rdr.Close();
                stm = String.Format("select timestamp_received,volume_received,vault_name from bankvault natural join recipient where recipient.id={0} order by timestamp_received desc", id);
                cmd = new MySqlCommand(stm, conn);
                rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    bloodTransactions.Add(String.Format("{0};{1};{2};{3}", rdr.GetString(0), rdr.GetString(1), rdr.GetString(2), ACTION_RECEIVE));
                }
                rdr.Close();
                bloodTransactions.Sort();
                bloodTransactions.Reverse();
                logRefreshSuccess = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                logRefreshSuccess = false;
            }
            mDatabaseManager.closeConnection();
        }

        private void mLogCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            mLogList.Items.Clear();
            populateLogList();
        }

        private void mClearLog_Click(object sender, EventArgs e)
        {
            mLogList.Items.Clear();
            bloodTransactions.Clear();
        }

    }
}
