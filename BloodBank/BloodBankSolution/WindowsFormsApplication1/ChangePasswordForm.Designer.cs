﻿namespace BloodBank
{
    partial class ChangePasswordForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangePasswordForm));
            this.mOldPassword = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.mNewPassword2 = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.mNewPassword = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.mInvalidPasswordLabel = new MaterialSkin.Controls.MaterialLabel();
            this.mInvalidNewPasswordLabel = new MaterialSkin.Controls.MaterialLabel();
            this.mNonEqualPasswordsLabel = new MaterialSkin.Controls.MaterialLabel();
            this.mChangePassword = new MaterialSkin.Controls.MaterialRaisedButton();
            this.SuspendLayout();
            // 
            // mOldPassword
            // 
            this.mOldPassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mOldPassword.Depth = 0;
            this.mOldPassword.Hint = "Old password";
            this.mOldPassword.Location = new System.Drawing.Point(12, 79);
            this.mOldPassword.MouseState = MaterialSkin.MouseState.HOVER;
            this.mOldPassword.Name = "mOldPassword";
            this.mOldPassword.PasswordChar = '\0';
            this.mOldPassword.SelectedText = "";
            this.mOldPassword.SelectionLength = 0;
            this.mOldPassword.SelectionStart = 0;
            this.mOldPassword.Size = new System.Drawing.Size(604, 23);
            this.mOldPassword.TabIndex = 0;
            this.mOldPassword.UseSystemPasswordChar = true;
            // 
            // mNewPassword2
            // 
            this.mNewPassword2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mNewPassword2.Depth = 0;
            this.mNewPassword2.Hint = "Enter new password again";
            this.mNewPassword2.Location = new System.Drawing.Point(12, 175);
            this.mNewPassword2.MouseState = MaterialSkin.MouseState.HOVER;
            this.mNewPassword2.Name = "mNewPassword2";
            this.mNewPassword2.PasswordChar = '\0';
            this.mNewPassword2.SelectedText = "";
            this.mNewPassword2.SelectionLength = 0;
            this.mNewPassword2.SelectionStart = 0;
            this.mNewPassword2.Size = new System.Drawing.Size(604, 23);
            this.mNewPassword2.TabIndex = 1;
            this.mNewPassword2.UseSystemPasswordChar = true;
            // 
            // mNewPassword
            // 
            this.mNewPassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mNewPassword.Depth = 0;
            this.mNewPassword.Hint = "New password";
            this.mNewPassword.Location = new System.Drawing.Point(12, 127);
            this.mNewPassword.MouseState = MaterialSkin.MouseState.HOVER;
            this.mNewPassword.Name = "mNewPassword";
            this.mNewPassword.PasswordChar = '\0';
            this.mNewPassword.SelectedText = "";
            this.mNewPassword.SelectionLength = 0;
            this.mNewPassword.SelectionStart = 0;
            this.mNewPassword.Size = new System.Drawing.Size(604, 23);
            this.mNewPassword.TabIndex = 2;
            this.mNewPassword.UseSystemPasswordChar = true;
            // 
            // mInvalidPasswordLabel
            // 
            this.mInvalidPasswordLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mInvalidPasswordLabel.AutoSize = true;
            this.mInvalidPasswordLabel.Depth = 0;
            this.mInvalidPasswordLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.mInvalidPasswordLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mInvalidPasswordLabel.Location = new System.Drawing.Point(8, 105);
            this.mInvalidPasswordLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.mInvalidPasswordLabel.Name = "mInvalidPasswordLabel";
            this.mInvalidPasswordLabel.Size = new System.Drawing.Size(347, 19);
            this.mInvalidPasswordLabel.TabIndex = 3;
            this.mInvalidPasswordLabel.Text = "Invalid password. Try again with correct password.";
            this.mInvalidPasswordLabel.Visible = false;
            // 
            // mInvalidNewPasswordLabel
            // 
            this.mInvalidNewPasswordLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mInvalidNewPasswordLabel.AutoSize = true;
            this.mInvalidNewPasswordLabel.Depth = 0;
            this.mInvalidNewPasswordLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.mInvalidNewPasswordLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mInvalidNewPasswordLabel.Location = new System.Drawing.Point(8, 153);
            this.mInvalidNewPasswordLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.mInvalidNewPasswordLabel.Name = "mInvalidNewPasswordLabel";
            this.mInvalidNewPasswordLabel.Size = new System.Drawing.Size(543, 19);
            this.mInvalidNewPasswordLabel.TabIndex = 4;
            this.mInvalidNewPasswordLabel.Text = "Invalid password. It should be at least 3 characters long  and alphanumeric only." +
    "";
            this.mInvalidNewPasswordLabel.Visible = false;
            // 
            // mNonEqualPasswordsLabel
            // 
            this.mNonEqualPasswordsLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mNonEqualPasswordsLabel.AutoSize = true;
            this.mNonEqualPasswordsLabel.Depth = 0;
            this.mNonEqualPasswordsLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.mNonEqualPasswordsLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mNonEqualPasswordsLabel.Location = new System.Drawing.Point(8, 201);
            this.mNonEqualPasswordsLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.mNonEqualPasswordsLabel.Name = "mNonEqualPasswordsLabel";
            this.mNonEqualPasswordsLabel.Size = new System.Drawing.Size(170, 19);
            this.mNonEqualPasswordsLabel.TabIndex = 5;
            this.mNonEqualPasswordsLabel.Text = "Passwords don\'t match.";
            this.mNonEqualPasswordsLabel.Visible = false;
            // 
            // mChangePassword
            // 
            this.mChangePassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mChangePassword.Depth = 0;
            this.mChangePassword.Location = new System.Drawing.Point(12, 245);
            this.mChangePassword.MouseState = MaterialSkin.MouseState.HOVER;
            this.mChangePassword.Name = "mChangePassword";
            this.mChangePassword.Primary = true;
            this.mChangePassword.Size = new System.Drawing.Size(166, 23);
            this.mChangePassword.TabIndex = 6;
            this.mChangePassword.Text = "Change Password";
            this.mChangePassword.UseVisualStyleBackColor = true;
            this.mChangePassword.Click += new System.EventHandler(this.mChangePassword_Click);
            // 
            // ChangePasswordForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(628, 509);
            this.Controls.Add(this.mChangePassword);
            this.Controls.Add(this.mNonEqualPasswordsLabel);
            this.Controls.Add(this.mInvalidNewPasswordLabel);
            this.Controls.Add(this.mInvalidPasswordLabel);
            this.Controls.Add(this.mNewPassword);
            this.Controls.Add(this.mNewPassword2);
            this.Controls.Add(this.mOldPassword);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ChangePasswordForm";
            this.Text = "Change Password";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialSingleLineTextField mOldPassword;
        private MaterialSkin.Controls.MaterialSingleLineTextField mNewPassword2;
        private MaterialSkin.Controls.MaterialSingleLineTextField mNewPassword;
        private MaterialSkin.Controls.MaterialLabel mInvalidPasswordLabel;
        private MaterialSkin.Controls.MaterialLabel mInvalidNewPasswordLabel;
        private MaterialSkin.Controls.MaterialLabel mNonEqualPasswordsLabel;
        private MaterialSkin.Controls.MaterialRaisedButton mChangePassword;
    }
}