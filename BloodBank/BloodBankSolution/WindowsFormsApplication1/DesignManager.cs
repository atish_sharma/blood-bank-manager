﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Animations;
using MaterialSkin.Controls;

namespace BloodBank
{
    class DesignManager
    {

        public static void setDesign(MaterialForm form)
        {
            MaterialSkinManager mMaterialSkinManager;
            mMaterialSkinManager = MaterialSkinManager.Instance;
            mMaterialSkinManager.AddFormToManage(form);
            mMaterialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            mMaterialSkinManager.ColorScheme = new ColorScheme(Primary.Purple800, Primary.Purple900, Primary.Purple500, Accent.Purple200, TextShade.WHITE);
        }

        public static void transition(Panel panel, ArrayList panels)
        {
            foreach (Panel p in panels)
            {
                if (p.Equals(panel))
                {
                    p.Visible = true;
                }
                else
                {
                    p.Visible = false;
                }
            }
        }

        public static void message(string title,string content)
        {
            MessageBox.Show(content, title);
        }

        public static void message(string title, string content,MessageBoxIcon icon)
        {
            MessageBox.Show(content, title,MessageBoxButtons.OK,icon);
        }

    }
}
