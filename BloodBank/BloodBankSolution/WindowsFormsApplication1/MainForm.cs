﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Animations;
using MaterialSkin.Controls;
using MySql.Data.MySqlClient;

namespace BloodBank
{
    public partial class MainForm : MaterialForm
    {

        public static string NO_BB = "(no blood bank selected)";
        public static string mPassword;
        public static string[] mUserSearchOptionsList = { "ID","Name"};
        public static string[] mBloodType = { "A+", "A-", "AB+", "AB-", "B+", "B-", "O+", "O-" };
        private AsyncResultManager mAsyncResultManager;
        private DatabaseManager mDatabaseManager;
        private ArrayList mPanels;
        private Boolean mBloodBankSuccess;
        private ArrayList mBloodBankResult;
        private string mSelectedBankId="";
        private int mSelectedBankIndex;
        private Boolean mAcquireSuccess;
        private string prevBloodRetrievalQuery;

        public MainForm()
        {
            InitializeComponent();
            mPanels = new ArrayList(new Panel[] 
                {
                    mLoginPanel,
                    mHomePanel
                }
            );
            mPassword = Properties.Settings.Default.Password;
            mDatabaseManager = DatabaseManager.getInstance();
            mAsyncResultManager = AsyncResultManager.getInstance();
            DesignManager.setDesign(this);
            mUserSearchOptions.Items.AddRange(mUserSearchOptionsList);
            mUserSearchOptions.SelectedIndex = 0;
            mBloodBankBloodType.Items.AddRange(mBloodType);
            mBloodBankBloodType.SelectedIndex = 0;
            mBloodBankResult = new ArrayList();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            DesignManager.transition(mLoginPanel, mPanels);
        }

        private void mExitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void mLoginPanel_Paint(object sender, PaintEventArgs e)
        {
            mInvalidPasswordLabel.Visible = false;
            mInvalidPasswordLabel.ForeColor = Color.Red;
        }

        private void mLoginButton_Click(object sender, EventArgs e)
        {
            if (mLoginPassword.Text.Trim().Equals(mPassword)) 
            {
                mInvalidPasswordLabel.Visible = false;
                mLoginPassword.Text = "";
                DesignManager.transition(mHomePanel,mPanels);
            }
            else
            {
                mInvalidPasswordLabel.Visible = true;
            }
        }

        private void mLogOut_Click(object sender, EventArgs e)
        {
            DesignManager.transition(mLoginPanel, mPanels);
        }

        private void mExitSettings_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void mClearUserSearch_Click(object sender, EventArgs e)
        {
            mUserSearchField.Text = "";
            mUserSearchResultList.Items.Clear();
        }

        private async void mSearchUser_Click(object sender, EventArgs e)
        {
            string value = mUserSearchField.Text.Trim();
            if(value.Equals(""))
            {
                DesignManager.message("Error", "Search field is empty.",MessageBoxIcon.Error);
                return;
            }
            int type=mUserSearchOptions.SelectedIndex;
            long n;
            if(type==0 && !Int64.TryParse(value,out n))
            {
                DesignManager.message("Error", "Please enter an integer in the search field.", MessageBoxIcon.Error);
                return;
            }
            mUserSearchProgress.Visible = true;
            mAsyncResultManager.mSearchUsersQuerySuccessful = false;
            await Task.Factory.StartNew(() => searchUsers(value,type), TaskCreationOptions.LongRunning);
            mUserSearchProgress.Visible = false;
            if(mAsyncResultManager.mSearchUsersQuerySuccessful)
            {
                if (mAsyncResultManager.mSearchUsersQueryResult.Count == 0)
                {
                    DesignManager.message("Uh Oh...", "It looks like you have 0 search results. Try searching something else.", MessageBoxIcon.Information);
                    return;
                }
                mUserSearchResultList.Items.Clear();
                foreach(User user in mAsyncResultManager.mSearchUsersQueryResult)
                {
                    ListViewItem item = new ListViewItem(user.Id.ToString());
                    item.SubItems.Add(UserForm.cstring(user.FirstName));
                    item.SubItems.Add(UserForm.cstring(user.LastName));
                    item.SubItems.Add(user.Dob);
                    item.SubItems.Add(user.BloodType);
                    item.SubItems.Add(UserForm.cstring(user.Street));
                    item.SubItems.Add(user.PinCode);
                    item.SubItems.Add(UserForm.cstring(user.City));
                    item.SubItems.Add(UserForm.cstring(user.District));
                    item.SubItems.Add(UserForm.cstring(user.State));
                    mUserSearchResultList.Items.Add(item);
                }
            }
            else
            {
                DesignManager.message("Error", "Unable to query database! Please check your internet connection.", MessageBoxIcon.Error);
            }
        }

        private void mSearchUsersResultList_Select(object sender, EventArgs e)
        {
            if (mUserSearchResultList.SelectedItems.Count > 0)
            {
                UserForm form = new UserForm(mUserSearchResultList.SelectedItems[0].Text);
                form.Show();
                form.BringToFront();
                this.SendToBack();
            }
        }

        private void mBloodBankList_Select(object sender, EventArgs e)
        {
            if (mBloodBankList.SelectedItems.Count > 0)
            {
                string name=mBloodBankList.SelectedItems[0].Text.ToLower();
                for(int i=0;i<mBloodBankResult.Count;i++)
                {
                    string s = (string)mBloodBankResult[i];
                    s = s.ToLower();
                    if (s.Split(';')[0].Equals(name)) 
                    {
                        mSelectedBankId=s.Split(';')[8];
                        mSelectedBankIndex = i;
                        break;
                    }
                }
                try
                {
                    mBloodBankName.Text = ((string)mBloodBankResult[mSelectedBankIndex]).Split(';')[0];
                    mBloodBankName.Text = UserForm.cstring(mBloodBankName.Text);
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }

        private void searchUsers(string value,int type)
        {
            MySqlConnection conn=mDatabaseManager.getConnection();
            if (conn == null) 
            {
                return;
            }
            mAsyncResultManager.mSearchUsersQueryResult = new ArrayList();
            try
            {
                value = value.ToLower();
                string stm = "select id,firstname,lastname,dob,blood_type,street,pincode,city,district,state from person ";
                if(type==0)
                {
                    stm += String.Format("where id = {0} ",value);
                }
                else
                {
                    stm += String.Format("where firstname like '%{0}%' or lastname like '%{0}%' ",value);
                }
                MySqlCommand cmd = new MySqlCommand(stm, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    User user = new User(Convert.ToInt64(rdr.GetString(0)), rdr.GetString(1), rdr.GetString(2), rdr.GetString(3), rdr.GetString(4), rdr.GetString(5), rdr.GetString(6), rdr.GetString(7), rdr.GetString(8), rdr.GetString(9));
                    mAsyncResultManager.mSearchUsersQueryResult.Add(user);
                }
                rdr.Close();
                foreach (User user in mAsyncResultManager.mSearchUsersQueryResult)
                {
                    stm = "select phone from personphone where id = " + user.Id;
                    user.phone = new ArrayList();
                    cmd = new MySqlCommand(stm, conn);
                    rdr = cmd.ExecuteReader();
                    while(rdr.Read())
                    {
                        user.phone.Add(rdr.GetString(0));
                    }
                    rdr.Close();
                }
                foreach (User user in mAsyncResultManager.mSearchUsersQueryResult)
                {
                    stm = "select email from personemail where id = " + user.Id;
                    user.email = new ArrayList();
                    cmd = new MySqlCommand(stm, conn);
                    rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        user.email.Add(rdr.GetString(0));
                    }
                    rdr.Close();
                }
                stm = "select bloodbank.bank_id,street,pincode,city,state,district,vault_name from bloodbank natural join bankvault";
                cmd = new MySqlCommand(stm, conn);
                rdr = cmd.ExecuteReader();
                mAsyncResultManager.mBloodBanks = new ArrayList();
                while(rdr.Read())
                {
                    BloodBank bloodBank = new BloodBank(Int64.Parse(rdr.GetString(0)), rdr.GetString(1), rdr.GetString(2), rdr.GetString(3), rdr.GetString(5), rdr.GetString(4), rdr.GetString(6));
                    mAsyncResultManager.mBloodBanks.Add(bloodBank);
                }
                rdr.Close();
                mAsyncResultManager.mSearchUsersQuerySuccessful = true;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
                mAsyncResultManager.mSearchUsersQuerySuccessful = false;
            }
            mDatabaseManager.closeConnection();
        }

        private void mAddUserButton_Click(object sender, EventArgs e)
        {
            AddUserForm form = new AddUserForm();
            form.Show();
        }

        private void mChangePassword_Click(object sender, EventArgs e)
        {
            ChangePasswordForm form = new ChangePasswordForm();
            form.Show();
        }

        private void mSearchByLocation_CheckedChanged(object sender, EventArgs e)
        {
            if(mSearchByLocation.Checked)
            {
                mBloodBankState.Enabled = true;
                mBloodBankCity.Enabled = true;
            }
            else
            {
                mBloodBankState.Enabled = false;
                mBloodBankCity.Enabled = false;
            }
        }

        private async void mBloodBankSearch_Click(object sender, EventArgs e)
        {
            Boolean error = false;
            string err = "";
            string state="",city="";
            string bloodType = mBloodBankBloodType.SelectedItem.ToString();
            if (mSearchByLocation.Checked)
            {
                state = mBloodBankState.Text.Trim().ToLower();
                city = mBloodBankCity.Text.Trim().ToLower(); 
                if(state.Equals(""))
                {
                    error = true;
                    err += "Please enter state.\n";
                }
                if (city.Equals(""))
                {
                    error = true;
                    err += "Please enter city.\n";
                }
            }
            if(bloodType.Equals(""))
            {
                error = true;
                err += "Please select blood type.\n";
            }
            if(error)
            {
                DesignManager.message("Error", err, MessageBoxIcon.Error);
                return;
            }
            mBloodBankSuccess = false;
            mSearchProgress.Visible = true;
            await Task.Factory.StartNew(() => retrieveBloodBank(bloodType, city, state, mSearchByLocation.Checked), TaskCreationOptions.LongRunning);
            mSearchProgress.Visible = false;
            if(mBloodBankSuccess)
            {
                if (mBloodBankResult.Count == 0)
                {
                    DesignManager.message("Uh Oh...", "It looks like you have 0 search results. Try searching something else.", MessageBoxIcon.Information);
                    return;
                }
                mSelectedBankId = "";
                mBloodBankName.Text = NO_BB;
                mBloodBankList.Items.Clear();
                populateBloodBankList();
            }
            else
            {
                DesignManager.message("Error", "Unable to query database! Please check your internet connection.", MessageBoxIcon.Error);
            }
        }

        private void populateBloodBankList()
        {
            foreach(string str in mBloodBankResult)
            {
                string[] s = str.Split(';');
                string name = UserForm.cstring(s[0]);
                string type = s[1];
                string vol = UserForm.cstring(s[2]);
                string street = UserForm.cstring(s[3]);
                string district = UserForm.cstring(s[4]);
                string pincode = UserForm.cstring(s[5]);
                string city = UserForm.cstring(s[6]);
                string state = UserForm.cstring(s[7]);
                ListViewItem item = new ListViewItem(name);
                item.SubItems.Add(type);
                item.SubItems.Add(vol);
                item.SubItems.Add(street);
                item.SubItems.Add(district);
                item.SubItems.Add(pincode);
                item.SubItems.Add(city);
                item.SubItems.Add(state);
                mBloodBankList.Items.Add(item);
            }
        }

        private void retrieveBloodBank(string type,string city,string state,Boolean loc)
        {
            MySqlConnection conn = mDatabaseManager.getConnection();
            if (conn == null)
            {
                return;
            }
            try
            {
                string stm = String.Format("select bloodbank.bank_id,street,pincode,city,state,district,vault_name,blood_type,blood_volume from bloodvault natural join bloodbank natural join bankvault where blood_type='{0}' and blood_volume > 0 ",type);
                if (loc)
                {
                    stm += String.Format("and city = '{0}' and state = '{1}'", city,state);
                }
                prevBloodRetrievalQuery = stm;
                MySqlCommand cmd = new MySqlCommand(stm, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                mBloodBankResult = new ArrayList();
                while (rdr.Read())
                {
                    mBloodBankResult.Add(String.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8}", rdr.GetString(6), rdr.GetString(7), rdr.GetString(8), rdr.GetString(1), rdr.GetString(5), rdr.GetString(2), rdr.GetString(3), rdr.GetString(4), rdr.GetString(0)));
                }
                rdr.Close();
                mBloodBankSuccess = true;
            }
            catch(Exception e)
            {
                mBloodBankSuccess = false;
                Console.WriteLine(e.ToString());
            }
            mDatabaseManager.closeConnection();
        }

        private void acquire(string type,long vol,long pid,long bid)
        {
            MySqlConnection conn = mDatabaseManager.getConnection();
            if (conn == null)
            {
                return;
            }
            long timestamp = (long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            try
            {
                string stm = String.Format("insert into recipient (id,volume_received,timestamp_received,bank_id) values ({0},{1},{2},{3})", pid, vol, timestamp, bid);
                MySqlCommand cmd = new MySqlCommand(stm, conn);
                cmd.ExecuteNonQuery();
                stm = String.Format("update bloodvault set blood_volume=blood_volume-{0} where bank_id={1} and blood_type='{2}'", vol, bid, type);
                cmd = new MySqlCommand(stm, conn);
                cmd.ExecuteNonQuery();
                cmd = new MySqlCommand(prevBloodRetrievalQuery, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                mBloodBankResult = new ArrayList();
                while (rdr.Read())
                {
                    mBloodBankResult.Add(String.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8}", rdr.GetString(6), rdr.GetString(7), rdr.GetString(8), rdr.GetString(1), rdr.GetString(5), rdr.GetString(2), rdr.GetString(3), rdr.GetString(4), rdr.GetString(0)));
                }
                rdr.Close();
                mAcquireSuccess = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                mAcquireSuccess = false;
            }
            mDatabaseManager.closeConnection();
        }

        private async void mAcquire_Click(object sender, EventArgs e)
        {
            if(mSelectedBankId.Equals(""))
            {
                DesignManager.message("Oops!", "Please select a blood bank to acquire blood.", MessageBoxIcon.Information);
            }
            else
            {
                try 
                { 
                    long bid = Int64.Parse(mSelectedBankId);
                    string Pid = mUserId.Text.Trim();
                    string vol = mBloodVolume.Text.Trim();
                    Boolean error = false;
                    string err = "";
                    long n;
                    if(vol.Equals(""))
                    {
                        error = true;
                        err+="Please enter volume.\n";
                    }
                    else if(!Int64.TryParse(vol,out n))
                    {
                        error = true;
                        err+="Please enter an integer as volume.\n";
                    }
                    if (Pid.Equals(""))
                    {
                        error = true;
                        err += "Please enter user ID.\n";
                    }
                    else if (!Int64.TryParse(Pid, out n))
                    {
                        error = true;
                        err += "Please enter an integer as user ID.\n";
                    }
                    if(error)
                    {
                        DesignManager.message("Error", err, MessageBoxIcon.Error);
                        return;
                    }
                    long Vol = Int64.Parse(vol);
                    Console.WriteLine(((string)mBloodBankResult[mSelectedBankIndex]).Split(';')[2]);
                    if(Vol>(long)Double.Parse(((string)mBloodBankResult[mSelectedBankIndex]).Split(';')[2]))
                    {
                        DesignManager.message("Error", "Insufficient blood amount. Please try again later or select another blood bank.", MessageBoxIcon.Error);
                        return;
                    }
                    long pid = Int64.Parse(Pid);
                    mAcquireProgress.Visible = true;
                    mAcquireSuccess = false;
                    await Task.Factory.StartNew(() => acquire(((string)mBloodBankResult[mSelectedBankIndex]).Split(';')[1],Vol,pid,bid), TaskCreationOptions.LongRunning);
                    mAcquireProgress.Visible = false;
                    if(mAcquireSuccess)
                    {
                       mUserId.Text="";
                       mBloodVolume.Text="";
                       if (mBloodBankResult.Count == 0)
                       {
                           DesignManager.message("Uh Oh...", "It looks like you have 0 search results. Try searching something else.", MessageBoxIcon.Information);
                           return;
                       }
                       mSelectedBankId = "";
                       mBloodBankName.Text = NO_BB;
                       mBloodBankList.Items.Clear();
                       populateBloodBankList();
                       DesignManager.message("Blood Retrieved", "Blood acquired successfully.", MessageBoxIcon.Information);
                    }
                    else
                    {
                        DesignManager.message("Error", "Unable to query database! Please check your internet connection.", MessageBoxIcon.Error);
                    }
                }
                catch(Exception ex)
                {
                    DesignManager.message("Error", "It looks like something went wrong. Please try again.", MessageBoxIcon.Error);
                    Console.WriteLine(ex.ToString());
                }
            }
        }

    }
}
