﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloodBank
{
    class BloodBank
    {

        public long Id;
        public string Street;
        public string PinCode;
        public string City;
        public string District;
        public string State;
        public string VaultName;

        public BloodBank()
        {

        }

        public BloodBank(long id,string street,string pincode,string city,string district,string state,string vaultName)
        {
            Id = id;
            Street = street;
            City = city;
            District = district;
            State = state;
            VaultName = vaultName;
            PinCode = pincode;
        }

    }
}
