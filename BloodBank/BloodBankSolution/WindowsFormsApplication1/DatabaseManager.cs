﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data;

namespace BloodBank
{
    class DatabaseManager
    {
        private static DatabaseManager mDatabaseManager = null;
        private string mConnectionString;
        private MySqlConnection mConnection;

        private DatabaseManager() 
        {
            mConnectionString = ConfigurationManager.ConnectionStrings["DatabaseConnection"].ConnectionString;
        }

        public static DatabaseManager getInstance() 
        {
            if (mDatabaseManager == null) 
            {
                mDatabaseManager = new DatabaseManager();
            }
            return mDatabaseManager;
        }

        public MySqlConnection getConnection() 
        {
            closeConnection();
            try
            {
                mConnection = new MySqlConnection(mConnectionString);
                mConnection.Open();
                Console.WriteLine("MySQL version : {0}", mConnection.ServerVersion);
            }
            catch (Exception e) 
            {
                Console.WriteLine(e.ToString());   
            }
            return mConnection;
        }

        public void closeConnection() 
        {
            try
            {
                mConnection.Close();
                Console.WriteLine("MySQL connection closed");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

    }
}
