﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloodBank
{
    class AsyncResultManager
    {

        private static AsyncResultManager mAsyncResultMananger=null;
        public Boolean mSearchUsersQuerySuccessful;
        public ArrayList mSearchUsersQueryResult;
        public ArrayList mBloodBanks;

        private AsyncResultManager()
        {

        }

        public static AsyncResultManager getInstance()
        {
            if(mAsyncResultMananger==null)
            {
                mAsyncResultMananger = new AsyncResultManager();
            }
            return mAsyncResultMananger;
        }

    }
}
