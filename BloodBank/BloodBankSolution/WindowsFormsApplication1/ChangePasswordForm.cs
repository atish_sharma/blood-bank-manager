﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Animations;
using MaterialSkin.Controls;
using MySql.Data.MySqlClient;

namespace BloodBank
{
    public partial class ChangePasswordForm : MaterialForm
    {
        private string oldPassword, newPassword, newPassword2;

        public ChangePasswordForm()
        {
            InitializeComponent();
            DesignManager.setDesign(this);
        }

        private void mChangePassword_Click(object sender, EventArgs e)
        {
            mInvalidPasswordLabel.Visible = false;
            mInvalidNewPasswordLabel.Visible = false;
            mNonEqualPasswordsLabel.Visible = false;
            oldPassword = mOldPassword.Text.Trim();
            newPassword = mNewPassword.Text.Trim();
            newPassword2 = mNewPassword2.Text.Trim();
            if(MainForm.mPassword.Equals(oldPassword))
            {
                if(newPassword.Equals(newPassword2))
                {
                    if(isValidPassword(newPassword))
                    {
                        Properties.Settings.Default.Password=newPassword;
                        Properties.Settings.Default.Save();
                        MainForm.mPassword = Properties.Settings.Default.Password;
                        mOldPassword.Text = "";
                        mNewPassword.Text = "";
                        mNewPassword2.Text = "";
                        DesignManager.message("Password Changed", "Password change successful!", MessageBoxIcon.Information);
                    }
                    else
                    {
                        mInvalidNewPasswordLabel.Visible = true;
                        mInvalidNewPasswordLabel.ForeColor = Color.Red;
                    }
                }
                else
                {
                    mNonEqualPasswordsLabel.Visible = true;
                    mNonEqualPasswordsLabel.ForeColor = Color.Red;
                }
            }
            else
            {
                mInvalidPasswordLabel.Visible = true;
                mInvalidPasswordLabel.ForeColor = Color.Red;
            }
        }

        private Boolean isValidPassword(string pass)
        {
            if(pass.Length<3)
            {
                return false;
            }
            foreach (char c in pass)
            {
                if(!Char.IsLetterOrDigit(c))
                {
                    return false;
                }
            }
            return true;
        }

    }
}
