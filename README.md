# Euphemia (Blood Bank Management System) #

# Purpose #

Blood donation is supreme service to humanity and it saves precious life. On a daily basis, numerous accidents take place. At times, it is very difficult to find the right donor with the matching blood group causing the near and dear ones of the patient to run from pillar to post in search of blood. To resolve this problem is the main purpose of this project.

# Project Scope #

This software system will be a Blood Donation System for local blood banks. This system will be designed to find the right donor with the matching blood group, which would otherwise have to be performed by their near and dear ones of the patient.

# Operating Environment #

1. Windows 8 or higher
2. IDE tool: Visual Studio 2013 or higher
3. Working internet connection